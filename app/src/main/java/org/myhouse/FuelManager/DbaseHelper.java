package org.myhouse.FuelManager;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.security.SecureRandom;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Random;

public class DbaseHelper extends SQLiteOpenHelper
{

     public static synchronized DbaseHelper getInstance(Context context)
     {
          if (instance == null)
          {
               instance = new DbaseHelper((context.getApplicationContext()));
          }
          return instance;
     }

     private DbaseHelper(Context c)
     {
          super(c, DATABASE_NAME, null, DATABASE_VERSION);
     }


     @Override
     public void onCreate(SQLiteDatabase db)
     {
          Log.d("dbasehelper", "inside the oncreate");

          //create statement
          String createVehicleTable = "create table if not exists " + vehicle_table + " "
                  + " (" + vehicle_id + " integer primary key on conflict fail, "
                  + vehicle_description + " text )";
          db.execSQL(createVehicleTable);
          //Log.d("database", "created vehicle table");

          String createFuelMileageTable = "create table if not exists " + mileage_table
                  + " (" + mileage_id + " integer primary key on conflict fail ,  "
                  + " " + mileage_miles + " real, "
                  + " " + mileage_gallons + " real, "
                  + " " + mileage_cost + " real, "
                  + " " + mileage_date + " text, "
                  + " " + mileage_vehicle_id + " text not null, "
                  + " foreign key (" + mileage_vehicle_id + ") references " + vehicle_table + "(" + vehicle_id + ") "
                  + ")";
          db.execSQL(createFuelMileageTable);
          //Log.d("database", "created fuel mileage table");

          String createVehicleChangesTable = "create table if not exists " + vehicle_changes_table
                  + "( " + vehicle_changes_id + " integer unique, "
                  + " foreign key (" + vehicle_changes_id + ") references " + vehicle_table + "(" + vehicle_id + ") "
                  + ")";
          db.execSQL(createVehicleChangesTable);
          //Log.d("database", "create vehicle changes table");

          String createFuelMileageChangesTable = "create table if not exists " + fuel_mileage_changes_table
                  + " (" + fuel_mileage_id + " integer unique, "
                  + " foreign key (" + fuel_mileage_id + ") references " + mileage_table + " ( " + mileage_id + " ) "
                  + ")";
          db.execSQL(createFuelMileageChangesTable);

          //turn on foreign keys
          String fk = "pragma foreign_keys = on";
          db.execSQL(fk);
     }

     @Override
     public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion)
     {
          onCreate(db);
     }


     public void checkTables()
     {
          Log.d("dbasehelper", "check the tables");
          //onCreate(this);

          SQLiteDatabase db = instance.getWritableDatabase();

          //create statement
          String createVehicleTable = "create table if not exists " + vehicle_table + " "
                  + " (" + vehicle_id + " integer primary key on conflict fail, "
                  + vehicle_description + " text )";
          db.execSQL(createVehicleTable);
          //Log.d("database", "created vehicle table");

          String createFuelMileageTable = "create table if not exists " + mileage_table
                  + " (" + mileage_id + " integer primary key on conflict fail ,  "
                  + " " + mileage_miles + " real, "
                  + " " + mileage_gallons + " real, "
                  + " " + mileage_cost + " real, "
                  + " " + mileage_date + " text, "
                  + " " + mileage_vehicle_id + " text not null, "
                  + " foreign key (" + mileage_vehicle_id + ") references " + vehicle_table + "(" + vehicle_id + ") "
                  + " on delete cascade on update cascade "
                  + ")";
          db.execSQL(createFuelMileageTable);
          //Log.d("database", "created fuel mileage table");

          String createVehicleChangesTable = "create table if not exists " + vehicle_changes_table
                  + "( " + vehicle_changes_id + " integer unique, "
                  + " foreign key (" + vehicle_changes_id + ") references " + vehicle_table + "(" + vehicle_id + ") "
                  + " on delete cascade on update cascade "
                  + ")";
          db.execSQL(createVehicleChangesTable);
          //Log.d("database", "create vehicle changes table");

          String createFuelMileageChangesTable = "create table if not exists " + fuel_mileage_changes_table
                  + " (" + fuel_mileage_id + " integer unique, "
                  + " foreign key (" + fuel_mileage_id + ") references " + mileage_table + " ( " + mileage_id + " ) "
                  + " on delete cascade on update cascade "
                  + ")";
          db.execSQL(createFuelMileageChangesTable);

          //turn on foreign keys
          String fk = "pragma foreign_keys = on";
          db.execSQL(fk);
     }


     public Vehicle getVehicle(int vid)
     {
          Vehicle v = new Vehicle();

          //table, columns, selection,selectionArgs, groupBy, having, orderBy
          SQLiteDatabase database = instance.getWritableDatabase();
          Cursor cursor = database.query(vehicle_table,
                  new String[]{vehicle_id, vehicle_description},
                  vehicle_id + "=?",
                  new String[]{Integer.toString(vid)},
                  null,
                  null,
                  null,
                  null
          );
          if (cursor.moveToFirst())
          {
               v = new Vehicle(cursor);
          }
          cursor.close();
          return v;
     }

     public void deleteAllLocalVehicles()
     {
          SQLiteDatabase database = instance.getWritableDatabase();
          //database.delete(vehicle_table, null, new String[]{});


          database.delete(fuel_mileage_changes_table,null,null);
          database.delete(vehicle_changes_table,null,null);
          database.delete(mileage_table,null,null);
          database.delete(vehicle_table, null,null);

          Log.d("dbasehelper", "called the delete function");



     }

     public void addAllVehicles(JSONArray jArray)
     {
          SQLiteDatabase database = instance.getWritableDatabase();
          try
          {
               if (jArray.length() > 0)
               {
                    //database.delete(DbaseHelper.vehicle_table, null, null);
                    for (int i = 0; i < jArray.length(); i++)
                    {
                         JSONObject e = jArray.getJSONObject(i);

                         ContentValues v = new ContentValues();
                         v.put(vehicle_id, e.getInt("id"));
                         v.put(vehicle_description, e.getString("description"));
                         try
                         {
                              database.insert(vehicle_table, null, v);
                         }
                         catch(SQLException sqle)
                         {
                              Log.d("dbasehelper" +
                                      "" +
                                      "","addallvehicles: " + sqle.toString());
                         }
                    }
               }
          }
          catch (JSONException jsone)
          {
               Log.d("addallvehicles", "json exception happened..." + jsone.toString());
          }
     }


     public void deleteAllLocalMileage()
     {
          SQLiteDatabase database = instance.getWritableDatabase();
          database.delete(mileage_table, null, new String[]{});
          Log.d("delete all local mileage","all the local mileage data should be gone");
     }

     public void addAllFuelMileage(JSONArray jArray)
     {
          Log.d("dbasehelper","just called addAllFuelMileage");
          SQLiteDatabase database = instance.getWritableDatabase();
          try
          {
               //Log.d("getalldata_thread","in the try block");
               if (jArray.length() > 0)
               {
                    //Log.d("dbasehelper","in the if statement, how many: " + jArray.length());
                    int count = 0;
                    for (int i = 0; i < jArray.length(); i++)
                    {
                         //Log.d("getalldata_thread","top of loop: " + i);
                         JSONObject e = jArray.getJSONObject(i);
                         ContentValues v = new ContentValues();
                         v.put(mileage_id,e.getInt("id"));
                         v.put(mileage_miles,e.getDouble("miles"));
                         v.put(mileage_gallons,e.getDouble("gallons"));
                         v.put(mileage_cost,e.getDouble("cost"));
                         v.put(mileage_date,e.getString("fillup_date"));
                         v.put(mileage_vehicle_id,e.getInt("vehicle_id"));

                         //Log.d("syncfuelmileage", "add new record: " + v.toString());
                         //database.insertWithOnConflict(DbaseHelper.mileage_table,null,v,SQLiteDatabase.CONFLICT_IGNORE);
                         try
                         {
                              //database.insertOrThrow(DbaseHelper.mileage_table, null, v);
                              database.insertOrThrow(mileage_table,null,v);
                         }
                         catch(Exception exception)
                         {
                              Log.d("dbasehelper", "exception addallfuelmileage: " + exception.toString());
                         }
                         //Log.d("syncfuelmileage","return value: " + Long.toString(returnValue));
                         count = i;
                    }
                    //Log.d("getalldata_thread","after loop how many: " + count + " should be in there now");
               }
//               else
//               {
//                    Log.d("getalldata_thread","length is not > 0");
//               }
          }
          catch (JSONException jsone)
          {
               Log.d("dbasehelper",jsone.toString());
          }
          //Log.d("getalldata_thread","at the end of the addAllFuelMileage function");
     }

     public int getAllRecords()
     {
          int total = 0;
          SQLiteDatabase db = instance.getReadableDatabase();
          Cursor c = db.rawQuery("select count(*) from fuel_mileage",null);
          if (c.moveToFirst())
          {
               do
               {
                    total = c.getInt(0);
               }while(c.moveToNext());
          }
          return total;
     }


     public ArrayList<Mileage> getMileage(Vehicle v)
     {
          ArrayList<Mileage> items = new ArrayList<>(0);
          //           query(String table, String[] columns, String selection, String[] selectionArgs, String groupBy, String having, String orderBy)
          SQLiteDatabase database = instance.getReadableDatabase();
          Cursor cursor = database.query(DbaseHelper.mileage_table,
                  new String[]{DbaseHelper.mileage_id, DbaseHelper.mileage_miles, DbaseHelper.mileage_gallons,
                          DbaseHelper.mileage_cost, DbaseHelper.mileage_date, DbaseHelper.mileage_vehicle_id},
                  DbaseHelper.mileage_vehicle_id + "= ?",
                  new String[]{Integer.toString(v.getVehicleId())},
                  null,
                  null,
                  DbaseHelper.mileage_date + " desc"
          );
          if (cursor.moveToFirst())
          {
               do
               {
                    Mileage m = new Mileage(cursor);
                    items.add(m);
               } while (cursor.moveToNext());
          }
          cursor.close();
          return items;
     }

     //@here,  this function doesnt do anything yet
     public long deleteMileageRecord(Mileage m)
     {
          long retVal = 0;
          return retVal;
     }

     public long updateVehicle(Vehicle old, Vehicle changed)
     {
          long retval = 0;
          SQLiteDatabase database = instance.getWritableDatabase();
          ContentValues v = new ContentValues();
          v.put(vehicle_id, changed.getVehicleId());
          v.put(vehicle_description, changed.getVehicleDescription());

          retval = database.update(vehicle_table, v, vehicle_id + "=? ",
                  new String[]{Integer.toString(old.getVehicleId())});

          //add a record to the changes table now
          v = new ContentValues();
          v.put(vehicle_changes_id,old.getVehicleId());
          retval = database.insertWithOnConflict(vehicle_changes_table,null,v,SQLiteDatabase.CONFLICT_IGNORE);
          return retval;
     }


     public ArrayList<String> getYears(Vehicle v)
     {
          ArrayList<String> years = new ArrayList<>();
          SQLiteDatabase database = instance.getReadableDatabase();
          Cursor cursor = database.rawQuery("select distinct strftime(\"%Y\",fillup_date) from " + DbaseHelper.mileage_table +
                          " where strftime(\"%Y\",fillup_date) != '0000' " + " and " + DbaseHelper.mileage_vehicle_id + " =?" +
                          " order by strftime(\"%Y\",fillup_date) desc",
                  new String[]{Integer.toString(v.getVehicleId())});
          if (cursor.moveToFirst())
          {
               do
               {
                    String y = cursor.getString(0);
                    years.add(y);
               } while (cursor.moveToNext());
          }
          cursor.close();
          return years;
     }

     public ArrayList<ArrayList<String>> getMonthlyStats(Vehicle v)
     {
          ArrayList<ArrayList<String>> data = new ArrayList<>();
          String[] months = new String[]{
                  "01", "02", "03", "04", "05", "06", "07", "08", "09", "10", "11", "12"
          };
          String[] selectStuff = new String[]{
                  "count(id)", "sum(miles)", "sum(gallons)", "sum(cost)", "sum(miles)/sum(gallons)"
          };
          String where = DbaseHelper.mileage_vehicle_id + "=? and strftime(\"%Y\",fillup_date)=? " +
                  " and strftime(\"%m\",fillup_date)=? ";
          ArrayList<String> years = getYears(v);
          //Log.d("monthlystats","which table: " + DbaseHelper.mileage_table);
          SQLiteDatabase database = instance.getReadableDatabase();
          for (String month : months)
          {
               ArrayList<String> row = new ArrayList<>();
               for (String year : years)
               {
                    //query(String table, String[] columns, String selection, String[] selectionArgs, String groupBy, String having, String orderBy)
                    Cursor c = database.query(DbaseHelper.mileage_table,
                            selectStuff,
                            where,
                            new String[]{Integer.toString(v.getVehicleId()), year, month}, null, null, null);

                    String sFillup = "0";

                    Double miles;
                    Double gallons;
                    Double cost;
                    Double mpg;

                    String sMiles = "0";
                    String sGallons = "0";
                    String sCost = "0";
                    String sMpg = "0";
                    String t = "";
                    if (c != null && c.moveToFirst())
                    {
                         if (!c.isNull(0))
                         {
                              sFillup = c.getString(0);
                         }
                         if (!c.isNull(1))
                         {
                              miles = c.getDouble(1);
                              sMiles = milesFormat.format(miles);
                         }
                         if (!c.isNull(2))
                         {
                              gallons = c.getDouble(2);
                              sGallons = gallonsFormat.format(gallons);
                         }
                         if (!c.isNull(3))
                         {
                              cost = c.getDouble(3);
                              sCost = costFormat.format(cost);
                         }
                         if (!c.isNull(4))
                         {
                              mpg = c.getDouble(4);
                              sMpg = mpgFormat.format(mpg);
                         }
                    }
//                    else
//                    {
//                         //Log.d("something bad", "getmonthlystats crashed and burned");
//                    }
                    t += "Fillups: " + sFillup + "<br>";
                    t += "Miles: " + sMiles + "<br>";
                    t += "Gallons: " + sGallons + "<br>";
                    t += "MPG: " + sMpg + "<br>";
                    t += "Cost: " + sCost;
                    row.add(t);
                    c.close();
               }//end years
               data.add(row);
          }//end months
          //Log.d("monthlystats", "how many records: " + data.toString());
          return data;
     }


     public ArrayList<Vehicle> getAllVehicles()
     {
          ArrayList<Vehicle> vehicles = new ArrayList<>(0);
          //table, columns, selection,selectionArgs, groupBy, having, orderBy
          //get all the vehicles from the vehicles table first
          SQLiteDatabase database = instance.getReadableDatabase();
          Cursor cursor = database.query(vehicle_table,
                  new String[]{vehicle_id, vehicle_description},
                  null,
                  null,
                  null,
                  null, vehicle_description
          );
          if (cursor.moveToFirst())
          {
               do
               {
                    Vehicle v = new Vehicle(cursor);
                    vehicles.add(v);
               } while (cursor.moveToNext());
          }
          cursor.close();
          return vehicles;
     }

     public long deleteVehicle(Vehicle vehicle)
     {
          SQLiteDatabase database = instance.getWritableDatabase();
          long returnVal = database.delete(vehicle_table, vehicle_id + "=? ",
                  new String[]{Integer.toString(vehicle.getVehicleId())});
          return returnVal;
     }

     public long addVehicle(String d)
     {
          int number = generateRandomNumber();
          String goodnumber = "no";
          SQLiteDatabase database = instance.getWritableDatabase();
          int howmanytimes = 0;
          do
          {
               String q = "select count(*) from " + DbaseHelper.vehicle_table + " where " + DbaseHelper.vehicle_id
                       + " =?";
               Cursor c = database.rawQuery(q,new String[]{Integer.toString(number)});
               if (c.moveToFirst())
               {
                    int count = c.getInt(0);
                    if (count == 0)
                    {
                         goodnumber = "yes";
                    }
                    else
                    {
                         number = generateRandomNumber();
                    }
               }
               //only search for a unique number 10 times!!
               if (howmanytimes == 10)
               {
                    goodnumber = "yes";
                    number = -1;
               }
               howmanytimes++;
               c.close();
          }while(goodnumber.equals("no"));

          ContentValues cv = new ContentValues();
          cv.put("id",number);
          cv.put("description",d);
          long newRowId = -1;
          database.insert(vehicle_table,null,cv);

          //since we added a new vehicle, we need to add a record to the vehicle changes table
          cv = new ContentValues();
          cv.put("vehicle_id",number);
          newRowId = database.insert(vehicle_changes_table,null,cv);

          return newRowId;
     }

     public long updateRecord(Mileage m,Vehicle vehicle)
     {
          long retVal = 0;

          //update it in the original table
          ContentValues v = new ContentValues();
          v.put(DbaseHelper.mileage_miles, m.getMiles());
          v.put(DbaseHelper.mileage_gallons, m.getGallons());
          v.put(DbaseHelper.mileage_cost, m.getCost());
          v.put(DbaseHelper.mileage_date, m.getDate());
          //update (String table, ContentValues values, String whereClause, String[] whereArgs)
          SQLiteDatabase database = instance.getWritableDatabase();
          retVal = database.update(mileage_table, v, mileage_id + "= ?", new String[]{Integer.toString(m.getId())});

          //now add it to the fuel mileage changes table
          v = new ContentValues();
          v.put(fuel_mileage_id,m.getId());
          retVal = database.insertWithOnConflict(fuel_mileage_changes_table,null,v,SQLiteDatabase.CONFLICT_IGNORE);
          return retVal;
     }

     public long addData(Mileage m, Vehicle vh)
     {
          long retval = 0;
          SQLiteDatabase database = instance.getWritableDatabase();
          int number = generateRandomNumber();
          String goodnumber = "no";
          int howmanytimes = 0;
          do
          {
               String q = "select count(*) from " + mileage_table + " where " + mileage_id
                       + " =?";
               Cursor c = database.rawQuery(q,new String[]{Integer.toString(number)});
               if (c.moveToFirst())
               {
                    int count = c.getInt(0);
                    if (count == 0)
                    {
                         goodnumber = "yes";
                    }
                    else
                    {
                         number = generateRandomNumber();
                    }
               }
               //only search for a unique number 10 times!!
               if (howmanytimes == 10)
               {
                    goodnumber = "yes";
                    number = -1;
               }
               howmanytimes++;
               c.close();
          }while(goodnumber.equals("no"));

          ContentValues v = new ContentValues();
          v.put(mileage_id,number);
          v.put(mileage_miles, m.getMiles());
          v.put(mileage_gallons, m.getGallons());
          v.put(mileage_cost, m.getCost());
          v.put(mileage_date, m.getDate());
          v.put(mileage_vehicle_id, m.getVehicleId());

          retval = database.insert(mileage_table,null,v);

          //now insert a record into the changes table
          v = new ContentValues();
          v.put(fuel_mileage_id,number);
          retval = database.insert(fuel_mileage_changes_table,null,v);

          return retval;
     }//end addData

     public ArrayList<String> getLifetimeStats(Vehicle v)
     {
          ArrayList<String> data = new ArrayList<>();
          SQLiteDatabase database = instance.getReadableDatabase();

          String sFillup = "0";
          String sMiles = "0";
          String sGallons = "0";
          String sMpg = "0";
          String sCost = "0";

          String[] selectStuff = new String[]{
                  "count(id)", "sum(miles)", "sum(gallons)", "sum(cost)", "sum(miles)/sum(gallons)"
          };
          String where = DbaseHelper.mileage_vehicle_id + "=? ";
          Cursor c = database.query(DbaseHelper.mileage_table,
                  selectStuff,
                  where,
                  new String[]{Integer.toString(v.getVehicleId())}, null, null, null);
          if (c.moveToFirst())
          {
               if (!c.isNull(0))
               {
                    sFillup = c.getString(0);
               }
//            if (sFillup.equals("0"))
//                sFillup = "";
               if (!c.isNull(1))
               {
                    sMiles = c.getString(1);
               }
               if (!c.isNull(2))
               {
                    sGallons = c.getString(2);
               }
               if (!c.isNull(3))
               {
                    sCost = c.getString(3);
               }
               if (!c.isNull(4))
               {
                    sMpg = c.getString(4);
               }
               data.add(sFillup);
               data.add(sMiles);
               data.add(sGallons);
               data.add(sMpg);
               data.add(sCost);
          } else
          {
               //Log.d("something bad","getLifetimeStats either no data or crashed and burned");
               //probably no data

               data.add(sFillup);
               data.add(sMiles);
               data.add(sGallons);
               data.add(sMpg);
               data.add(sCost);
          }
          c.close();
          return data;
     }//end LifetimeStats

     public ArrayList<ArrayList<String>> getYearlyStats(Vehicle v)
     {
          ArrayList<ArrayList<String>> data = new ArrayList<>();

          String[] selectStuff = new String[]{
                  "strftime(\"%Y\",fillup_date)",
                  "count(id)", "sum(miles)", "sum(gallons)", "sum(cost)", "sum(miles)/sum(gallons)"
          };
          SQLiteDatabase database = instance.getReadableDatabase();
          String where = "";
          Cursor c;
          where = DbaseHelper.mileage_vehicle_id + "=? and strftime(\"%Y\",fillup_date)!='0000' ";
          c = database.query(DbaseHelper.mileage_table,
                  selectStuff,
                  where,
                  new String[]{Integer.toString(v.getVehicleId())},
                  "strftime(\"%Y\",fillup_date)",
                  null, "strftime(\"%Y\",fillup_date) desc");
          if (c.moveToFirst())
          {
               do
               {
                    //Toast.makeText(this,sYear,Toast.LENGTH_LONG).show();
                    //Toast.makeText(getBaseContext(),Integer.toString(v.getVehicleId()),Toast.LENGTH_LONG).show();
                    Double miles;
                    Double gallons;
                    Double cost;
                    Double mpg;

                    String sYear = "";
                    String sFillup = "";
                    String sMiles = "";
                    String sGallons = "";
                    String sCost = "";
                    String sMpg = "";
                    ArrayList<String> row = new ArrayList<>();
                    //Log.d("year:", sYear);
                    if (!c.isNull(0))
                    {
                         sYear = c.getString(0);
                    }
                    if (!c.isNull(1))
                    {
                         sFillup = c.getString(1);
                    }
                    if (!c.isNull(2))
                    {
                         miles = c.getDouble(2);
                         sMiles = milesFormat.format(miles);
                    }
                    if (!c.isNull(3))
                    {
                         gallons = c.getDouble(3);
                         sGallons = gallonsFormat.format(gallons);
                    }
                    if (!c.isNull(4))
                    {
                         cost = c.getDouble(4);
                         sCost = costFormat.format(cost);
                    }
                    if (!c.isNull(5))
                    {
                         mpg = c.getDouble(5);
                         sMpg = mpgFormat.format(mpg);
                    }
                    row.add(sYear);
                    row.add(sFillup);
                    row.add(sMiles);
                    row.add(sGallons);
                    row.add(sCost);
                    row.add(sMpg);

                    data.add(row);

               } while (c.moveToNext());
          }
          c.close();
          //database.close();
          return data;
     }//end getYearlyStats

     public JSONArray getVehicleChanges()
     {
          SQLiteDatabase database = instance.getReadableDatabase();
          JSONArray bigArray = new JSONArray();
          String q = "          select id,description\n" +
                  "          from vehicle_changes\n" +
                  "          join vehicles\n" +
                  "          on id = vehicle_id\n";
          Cursor c = database.rawQuery(q,null);
          if (c != null && c.moveToFirst())
          {
               do
               {
                    try
                    {
                         JSONObject obj = new JSONObject();
                         obj.put("id", c.getInt(0));
                         obj.put("description", c.getString(1));
                         bigArray.put(obj);
                    }
                    catch (JSONException jsone)
                    {

                    }
               }while(c.moveToNext());
               try
               {
                    c.close();
               }
               catch(NullPointerException npe)
               {

               }
          }
          //return bigArray.toString();
          return bigArray;
     }

     public int deleteVehicleChangesTable()
     {
          int rowsDeleted = 0;
          SQLiteDatabase database = instance.getWritableDatabase();
          rowsDeleted += database.delete(DbaseHelper.vehicle_changes_table,null,null);
          return rowsDeleted;
     }


     public boolean syncVehicles(JSONArray jArray)
     {
          SQLiteDatabase database = instance.getWritableDatabase();
          long returnValue = -1;
          boolean checkme = true;
          try
          {
               if (jArray.length() > 0)
               {
                    for (int i = 0; i < jArray.length(); i++)
                    {
                         JSONObject e = jArray.getJSONObject(i);

                         //get the vehicle id coming in
                         int id = e.getInt("id");

                         //           query(String table, String[] columns, String selection, String[] selectionArgs, String groupBy, String having, String orderBy)
                         Cursor cursor = database.query(vehicle_table,
                                 new String[]{"count(id)"},
                                 vehicle_id + "= ?",
                                 new String[]{Integer.toString(id)},
                                 null,
                                 null,
                                 null
                         );
                         int howmany = -1;
                         if (cursor.moveToFirst())
                         {
                              howmany = cursor.getInt(0);
                         }
                         cursor.close();
                         //if the vehicle id already exists,  update the record
                         if(howmany == 1)
                         {
                              ContentValues v = new ContentValues();
                              v.put(vehicle_description,e.getString("description"));
                              returnValue = database.update(vehicle_table, v, vehicle_id + "=? ",
                                      new String[]{Integer.toString(id)});
                              if (returnValue == -1)
                              {
                                   checkme = false;
                              }
                         }
                         //otherwise, insert a new record
                         else
                         {
                              ContentValues v = new ContentValues();
                              v.put(vehicle_id, e.getInt("id"));
                              v.put(vehicle_description, e.getString("description"));
                              returnValue = database.insert(vehicle_table, null, v);
                              if (returnValue == -1)
                              {
                                   checkme = false;
                              }
                         }
                    }
               }
          }
          catch (JSONException jsone)
          {
               //Log.d("mydbase","json exception happened...");
               //Log.d("mydbase",jsone.toString());
          }
          //database.close();
          return checkme;
     }

     public JSONArray getFuelMileageChanges()
     {
          JSONArray bigArray = new JSONArray();
          SQLiteDatabase database = instance.getReadableDatabase();

          String q = "          select id,miles,gallons,cost,fillup_date,vehicle_id \n" +
                  "          from fuel_mileage \n" +
                  "          join fuel_mileage_changes \n" +
                  "          on fm_id = id\n";
          Cursor c = database.rawQuery(q,null);
          if (c != null && c.moveToFirst())
          {
               do
               {
                    try
                    {
                         JSONObject obj = new JSONObject();
                         obj.put("id", c.getInt(0));
                         obj.put("miles", c.getDouble(1));
                         obj.put("gallons", c.getDouble(2));
                         obj.put("cost", c.getDouble(3));
                         obj.put("fillup_date", c.getString(4));
                         obj.put("vehicle_id",c.getInt(5));
                         bigArray.put(obj);
                    }
                    catch (JSONException jsone)
                    {

                    }
               }while(c.moveToNext());
               try
               {
                    c.close();
               }
               catch(NullPointerException npe)
               {

               }
          }
          return bigArray;
     }

     public int deleteFuelMileageChangesTable()
     {
          int rowsDeleted = 0;
          SQLiteDatabase database = instance.getWritableDatabase();
          rowsDeleted += database.delete(DbaseHelper.fuel_mileage_changes_table,null,null);
          return rowsDeleted;
     }

     public boolean syncFuelMileage(JSONArray jArray)
     {
          long returnValue = -1;
          SQLiteDatabase database = instance.getWritableDatabase();
          boolean checkme = true;
          try
          {
               if (jArray.length() > 0)
               {
                    for (int i = 0; i < jArray.length(); i++)
                    {
                         JSONObject e = jArray.getJSONObject(i);

                         //get the mileage id first
                         int id = e.getInt("id");
                         Cursor cursor = database.query(DbaseHelper.mileage_table,
                                 new String[]{"count(id)"},
                                 DbaseHelper.mileage_id + "= ?",
                                 new String[]{Integer.toString(id)},
                                 null,
                                 null,
                                 null
                         );
                         Log.d("syncfuelmileage","id to look for: " + e.getString("id"));
                         int howmany = -1;
                         if (cursor.moveToFirst())
                         {
                              howmany = cursor.getInt(0);
                         }
                         cursor.close();
                         Log.d("syncfuelmileage","howmany: " + howmany);
                         //if the fuel mileage id already exists,  update the record
                         if(howmany == 1)
                         {
                              ContentValues v = new ContentValues();
                              v.put(DbaseHelper.mileage_miles, e.getDouble("miles"));
                              v.put(DbaseHelper.mileage_gallons, e.getDouble("gallons"));
                              v.put(DbaseHelper.mileage_cost, e.getDouble("cost"));
                              v.put(DbaseHelper.mileage_date, e.getString("fillup_date"));

                              returnValue = database.update(DbaseHelper.mileage_table, v, DbaseHelper.mileage_id + "=? ",
                                      new String[]{Integer.toString(id)});
                              if (returnValue == -1)
                              {
                                   checkme = false;
                              }
                         }
                         else //otherwise, insert the record
                         {
                              JSONArray keys = e.names();
//                              StringBuilder t = new StringBuilder();
//                              for(int j=0;j<keys.length();j++)
//                              {
//                                   t.append(keys.getString(j));
//                                   t.append(e.getString(keys.getString(j)));
//                              }
//                              Log.d("syncfuelmileage", t.toString());

                              ContentValues v = new ContentValues();
                              v.put(DbaseHelper.mileage_id, e.getInt("id"));
                              v.put(DbaseHelper.mileage_miles, e.getDouble("miles"));
                              v.put(DbaseHelper.mileage_gallons, e.getDouble("gallons"));
                              v.put(DbaseHelper.mileage_cost, e.getDouble("cost"));
                              v.put(DbaseHelper.mileage_date, e.getString("fillup_date"));
                              v.put(DbaseHelper.mileage_vehicle_id, e.getInt("vehicle_id"));

                              returnValue = database.insertWithOnConflict(DbaseHelper.mileage_table,null,v,SQLiteDatabase.CONFLICT_FAIL);
                              Log.d("syncfuelmileage","return value: " + returnValue);
                              if (returnValue == -1)
                              {
                                   checkme = false;
                              }
                         }
                    }
               }
               else
               {
                    Log.d("syncfuelmileage", "incoming jsonarray <=0");
               }
          }
          catch (JSONException jsone)
          {
               Log.d("syncfuelmileage","json exception happened");
               Log.d("syncfuelmileage",jsone.toString());
               checkme = false;
          }
          //database.close();
          //Log.d("syncfuelmileage", "end of the function");
          return checkme;
     }


     private int generateRandomNumber()
     {
          long seed = System.currentTimeMillis();
          Random random = new SecureRandom();
          random.setSeed(seed);
          int theNumber = random.nextInt(1000000000); // 1 billion,  1,000,000,000
          return theNumber;
     }


     private static final String DATABASE_NAME = "fuelmanager.db";
     private static final int DATABASE_VERSION = 1;

     //table names
     public static final String vehicle_table = "vehicles";
     public static final String mileage_table = "fuel_mileage";
     public static final String vehicle_changes_table = "vehicle_changes";
     public static final String fuel_mileage_changes_table = "fuel_mileage_changes";

     //vehicle table column names
     public static final String vehicle_id = "id";
     public static final String vehicle_description = "description";

     //fuel_mileage column names
     public static final String mileage_id = "id";
     public static final String mileage_miles = "miles";
     public static final String mileage_gallons = "gallons";
     public static final String mileage_cost = "cost";
     public static final String mileage_date = "fillup_date";
     public static final String mileage_vehicle_id = "vehicle_id";

     //vehicle_changes column names
     public static final String vehicle_changes_id = "vehicle_id";

     //fuel_mileage_changes column names
     public static final String fuel_mileage_id = "fm_id";

     private static DbaseHelper instance;

     private final DecimalFormat milesFormat = new DecimalFormat("#,###.00");
     private final DecimalFormat gallonsFormat= new DecimalFormat("#,###.00");
     private final DecimalFormat costFormat= new DecimalFormat("\u00A4" + "#,###.00");
     private final DecimalFormat mpgFormat = new DecimalFormat("0.000");
}