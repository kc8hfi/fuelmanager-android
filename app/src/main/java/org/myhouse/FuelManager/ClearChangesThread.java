package org.myhouse.FuelManager;

import android.content.ContentValues;
import android.os.Handler;
import android.os.HandlerThread;
import android.os.Message;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.net.URL;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;

import javax.net.ssl.HttpsURLConnection;

public class ClearChangesThread extends HandlerThread
{
     private Handler mWorkerHandler;
     private Handler mResponseHandler;
     private ClearChangesThread.Callback mCallback;
     private static final String TAG = DownloadThread.class.getSimpleName();

     private String hostname;
     private String database;
     private String username;
     private String password;
     private String url;

     public ClearChangesThread(Handler responseHandler, ClearChangesThread.Callback callback)
     {
          super(TAG);
          mResponseHandler = responseHandler;
          mCallback = callback;
     }

     public interface Callback
     {
          void clearChangesHandler(int responsecode);
     }


     public void queueTask(String host, String db, String u, String p, String t)
     {
          hostname = host;
          database = db;
          username = u;
          password = p;
          url = t;
          mWorkerHandler.obtainMessage(1).sendToTarget();
     }



     public void prepareHandler()
     {
          mWorkerHandler = new Handler(getLooper(), new Handler.Callback()
          {
               public boolean handleMessage(Message msg)
               {
                    handleRequest();
                    return true;
               }
          });
     }

     private void handleRequest()
     {
          String webpage = "https://" + hostname + url;
          StringBuilder result = new StringBuilder();
          try
          {
               URL url = new URL(webpage);
               HttpsURLConnection httpclient = (HttpsURLConnection) url.openConnection();
               httpclient.setRequestMethod("POST");

               ContentValues credentials = new ContentValues();
               credentials.put("username", username);
               credentials.put("password", password);
               credentials.put("database", database);
               //set up the request to get the vehicles

               ArrayList<StringBuilder> stuff = new ArrayList<>();
               for(String key:credentials.keySet())
               {
                    StringBuilder t = new StringBuilder();
                    t.append(key);
                    t.append("=");
                    t.append(credentials.get(key));
                    stuff.add(t);
               }

               String sendme = String.join("&",stuff);

               OutputStream os = httpclient.getOutputStream();
               BufferedWriter writer = new BufferedWriter(new OutputStreamWriter(os, StandardCharsets.UTF_8));
               writer.write(sendme);
               writer.flush();
               writer.close();
               os.close();

               final int responseCode = httpclient.getResponseCode();
               if (responseCode == HttpsURLConnection.HTTP_OK)
               {
                    String line;
                    BufferedReader br = new BufferedReader(new InputStreamReader(httpclient.getInputStream()));
                    while ((line = br.readLine()) != null)
                    {
                         result.append(line);
                    }
               }
               mResponseHandler.post(new Runnable()
               {
                    @Override
                    public void run()
                    {
                         //mCallback.syncEverythingHandler(1,everything);
                         mCallback.clearChangesHandler(1);
                    }
               });
          }
          catch (IOException e)
          {
               e.printStackTrace();
          }
     }
}
