package org.myhouse.FuelManager;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;

public class Main extends Dashboard
{  //Activity  AppCompatActivity

     @Override
     protected void onCreate(Bundle savedInstanceState) {
          super.onCreate(savedInstanceState);
          setContentView(R.layout.activity_main);

          checkSettings();

          DbaseHelper db = DbaseHelper.getInstance(this);
          db.checkTables();
     }

     protected void onResume()
     {
          super.onResume();

          checkSettings();
     }

     protected void onActivityResult(int requestCode,int ResultCode, Intent data)
     {
          super.onActivityResult(requestCode,ResultCode,data);
          checkSettings();
     }

     private void checkSettings()
     {
          Vehicle vehicle = getVehicle();
          TextView t = findViewById(R.id.selectedVehicle);
          t.setText(vehicle.getVehicleDescription());
     }

     public void entryForm(View v)
     {
          //Toast.makeText(getApplicationContext(),"show entry form",Toast.LENGTH_LONG).show();
          Intent intent = new Intent(getApplicationContext(),EntryForm.class);
          startActivity(intent);
     }

     public void editData(View v)
     {
          //Toast.makeText(getApplicationContext(),"show edit data",Toast.LENGTH_LONG).show();
          Intent intent = new Intent(getApplicationContext(),ShowData.class);
          startActivity(intent);
     }

     public void monthlyStats(View v)
     {
          Intent intent = new Intent(getApplicationContext(), MonthlyStats.class);
          startActivity(intent);
     }

     public void yearlyStats(View v)
     {
          Intent intent = new Intent(getApplicationContext(),YearlyStats.class);
          startActivity(intent);
     }

     public void lifetimeStats(View v)
     {
          Intent intent = new Intent(getApplicationContext(),LifetimeStats.class);
          startActivity(intent);
     }
}