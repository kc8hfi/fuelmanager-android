package org.myhouse.FuelManager;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.EditText;

public class EditVehicleDescription extends Dashboard {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_edit_vehicle_description);

        description = findViewById(R.id.editDescription);
        Intent intent = getIntent();
        Bundle b = intent.getExtras();
        if (b != null)
        {
            current = new Vehicle(b);
            description.setText(current.getVehicleDescription());
            Log.d("updatevehicle", current.toString());
        }
    }

    public void saveChanges(View v)
    {
        //String message = "Please Fill out:\n";
        boolean ok = true;

        if (description.getText().toString().equals(""))
        {
            //message += "description\n";
            ok = false;
        }
        if (ok)
        {
            Vehicle changed = new Vehicle();
            changed.setVehicleId(current.getVehicleId());
            changed.setVehicleDescription(description.getText().toString());

            getDbaseHandle().updateVehicle(current,changed);

            int s = getVehicleID();
            //int s = pref.getVehicleID();
            //String l = pref.getVehicleLocation();
            Vehicle saved = getDbaseHandle().getVehicle(s);
            //change the saved preferences vehicle to the new code
            if (saved.getVehicleId() == current.getVehicleId())
            {
                //pref.setVehicleID(changed.getVehicleId());
                setVehicleID(changed.getVehicleId());

            }
            //everything is ok, update the vehicle
//            vehicle.setVehicleCode(code.getText().toString());
//            vehicle.setVehicleDescription(description.getText().toString());
//
//            //now, update the database table
//            database.updateVehicle(vehicle);
        }
        finish();
    }//end saveChanges

    public void cancelChanges(View v)
    {
        setResult(RESULT_CANCELED);
        finish();
    }

    private Vehicle current;
    private EditText description;
}