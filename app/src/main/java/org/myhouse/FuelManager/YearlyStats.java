package org.myhouse.FuelManager;

import android.content.Intent;
import android.os.Bundle;
import android.util.Base64;
import android.webkit.WebView;
import android.widget.Toast;
import java.util.ArrayList;

public class YearlyStats extends Dashboard
{
     @Override
     protected void onCreate(Bundle savedInstanceState)
     {
          super.onCreate(savedInstanceState);
          setContentView(R.layout.activity_yearly_stats);

          theView = findViewById(R.id.myView);
          theView.getSettings().setBuiltInZoomControls(true);
          theView.getSettings().setDisplayZoomControls(false);

          vehicle = getVehicle();
          if (vehicle.getVehicleId() != -1)
          {
               updateStats();
          } else
          {
               Toast.makeText(getApplicationContext(), "select a vehicle first!", Toast.LENGTH_LONG).show();
          }
     }

     private void updateStats()
     {
          ArrayList<String> headerNames = new ArrayList<>();
          headerNames.add("");
          headerNames.add("Fillups");
          headerNames.add("Miles");
          headerNames.add("Gallons");
          headerNames.add("Cost");
          headerNames.add("MPG");

          //"<!DOCTYPE HTML PUBLIC \"-//W3C//DTD HTML 4.01//EN\" \"http://www.w3.org/TR/html4/strict.dtd\">\n" +
          StringBuilder thepage = new StringBuilder();
          thepage.append(
                  "<!DOCTYPE HTML>\n" +
                          "<html>\n" +
                          "<title>sample</title>\n" +
                          "<style type=\"text/css\">\n" +
                          ".bgcolor\n" +
                          "{\n" +
                          "/*      background-color: #D8D8D8; */\n" +
                          "     background-color:   #c0dbea;\n" +
                          "}\n" +
                          "\n" +
                          "/* fix up the look of tables */\n" +
                          "/* table */\n" +
                          ".tabledata\n" +
                          "{\n" +
                          "     \n" +
                          "     /* show empty cells as regular cells */\n" +
                          "     empty-cells:   show;\n" +
                          "     border-width:  1px;\n" +
                          "     border-spacing:     2px;\n" +
                          "     border-collapse:    collapse;\n" +
                          "}\n" +
                          "\n" +
                          "/* table td,th  */\n" +
                          ".tabledata td,th\n" +
                          "{\n" +
                          "     /* make sure the table td fields don't wrap if they are long */\n" +
                          "     white-space: nowrap;\n" +
                          "     \n" +
                          "     border-width:  1px;\n" +
                          "     padding:  5px;\n" +
                          "     border-style:  solid;\n" +
                          "}\n" +
                          "\n" +
                          "</style>\n" +
                          "<body>\n" +
                          "\n" +
                          "<table class=\"tabledata\" > \n");


          //add the table column headers
          thepage.append("<tr class=\"bgcolor\">");
          for (String s : headerNames)
          {
               thepage.append("<th>").append(s).append("</th>");
          }
          thepage.append("</tr>\n");

          ArrayList<ArrayList<String>> data = getDbaseHandle().getYearlyStats(vehicle);
          int i = 1;
          for (ArrayList<String> row : data)
          {
               StringBuilder r = new StringBuilder();

               if ((i % 2) == 0)
               {
                    r.append("<tr class=\"bgcolor\">");
               } else
               {
                    r.append("<tr>");
               }
               //each item in the row goes in td tags
               for (String eachpart : row)
               {
                    r.append("<td>").append(eachpart).append("</td>");
               }
               r.append("</tr>\n");

               //add the row to the rest now
               thepage.append(r.toString());
               i++;
          }
          //add the ending now
          thepage.append("</table>\n").append("</body>\n").append("</html>\n");


          //clear the page first
          theView.loadUrl("about:blank");
          //put the page onto the webview
          String encodeMe = Base64.encodeToString(thepage.toString().getBytes(), Base64.NO_PADDING);
          theView.loadData(encodeMe, "text/html", "base64");
     }

     protected void onActivityResult(int requestCode, int ResultCode, Intent data)
     {
          super.onActivityResult(requestCode, ResultCode, data);
          vehicle = getVehicle();
          updateStats();
     }


     private WebView theView;
     private Vehicle vehicle;
}