package org.myhouse.FuelManager;

import android.content.ContentValues;
import android.os.Handler;
import android.os.HandlerThread;
import android.os.Message;
import android.util.Log;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.net.URL;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;

import javax.net.ssl.HttpsURLConnection;

public class UploadThread extends HandlerThread
{

     private Handler mWorkerHandler;
     private Handler mResponseHandler;
     private UploadThread.Callback mCallback;
     private static final String TAG = UploadThread.class.getSimpleName();

     private String hostname;
     private String database;
     private String username;
     private String password;
     private String url;

     private ArrayList<DataObject> theData;

     public UploadThread(String name)
     {
          super(name);
     }

     public UploadThread(Handler responseHandler, UploadThread.Callback callback)
     {
          super(TAG);
          mResponseHandler = responseHandler;
          mCallback = callback;
     }

     public interface Callback
     {
          void uploadEverythingHandler(int responsecode, StringBuilder status);
     }

     //ArrayList<DataObject> stuff =

     public void queueTask(String host, String db, String u, String p, ArrayList<DataObject> stuff,String t)
     {
          hostname = host;
          database = db;
          username = u;
          password = p;
          url = t;
          theData = stuff;
          mWorkerHandler.obtainMessage(1).sendToTarget();
     }


     public void prepareHandler()
     {
          mWorkerHandler = new Handler(getLooper(), new Handler.Callback()
          {
               public boolean handleMessage(Message msg)
               {
                    handleRequest();
                    return true;
               }
          });
     }



     private void handleRequest()
     {
          String webpage = "https://" + hostname + url;
          //everything.setWebpage(webpage);
          final StringBuilder result = new StringBuilder();
          try
          {
               for(int i=0;i<theData.size();i++)
               {
                    DataObject o = theData.get(i);

                    URL url = new URL(webpage);
                    HttpsURLConnection httpclient = (HttpsURLConnection) url.openConnection();
                    httpclient.setRequestMethod("POST");

                    ContentValues credentials = new ContentValues();
                    credentials.put("username", username);
                    credentials.put("password", password);
                    credentials.put("database", database);
                    //set up the request to get the vehicles
                    credentials.put("clickme", o.getWhichOne());
                    credentials.put("thedata",o.getTheData().toString());

                    Log.d(TAG, o.getTheData().toString());

                    ArrayList<StringBuilder> stuff = new ArrayList<>();
                    for(String key:credentials.keySet())
                    {
                         StringBuilder t = new StringBuilder();
                         t.append(key);
                         t.append("=");
                         t.append(credentials.get(key));
                         stuff.add(t);
                    }

                    String sendme = String.join("&",stuff);

                    OutputStream os = httpclient.getOutputStream();
                    BufferedWriter writer = new BufferedWriter(new OutputStreamWriter(os, StandardCharsets.UTF_8));
                    writer.write(sendme);
                    writer.flush();
                    writer.close();
                    os.close();

                    final int responseCode = httpclient.getResponseCode();
                    if (responseCode == HttpsURLConnection.HTTP_OK)
                    {
                         String line;
                         BufferedReader br = new BufferedReader(new InputStreamReader(httpclient.getInputStream()));
                         while ((line = br.readLine()) != null)
                         {
                              result.append(line);
                         }
                    }
               }//end loop
               mResponseHandler.post(new Runnable()
               {
                    @Override
                    public void run()
                    {
                         mCallback.uploadEverythingHandler(1,result);
                    }
               });
          }
          catch (IOException e)
          {
               e.printStackTrace();
               final StringBuilder error = new StringBuilder();
               StackTraceElement[] array = e.getStackTrace();

               error.append(e.getMessage());
               mResponseHandler.post(new Runnable()
               {
                    @Override
                    public void run()
                    {
                         mCallback.uploadEverythingHandler(1, error);
                    }
               });
          }
     }
}
