package org.myhouse.FuelManager;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import androidx.core.content.ContextCompat;

import android.view.Gravity;
import android.view.View;
import android.widget.TableLayout;
import android.widget.TableRow;
import android.widget.TableRow.LayoutParams;
import android.widget.TextView;
import android.widget.Toast;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Locale;

public class ShowData extends Dashboard
{
     @Override
     protected void onCreate(Bundle savedInstanceState)
     {
          super.onCreate(savedInstanceState);
          setContentView(R.layout.activity_show_data);

          values = new ArrayList<>();
          thetable = findViewById(R.id.table);

          vehicle = getVehicle();
          if (vehicle.getVehicleId() != -1)
          {
               updateTable();
          }
          else
          {
               Toast.makeText(getApplicationContext(),"select a vehicle first!",Toast.LENGTH_LONG).show();
          }
     }


     private void updateTable()
     {
          //get the mileage info for the vehicle
          values.clear();
          values = getDbaseHandle().getMileage(vehicle);

          thetable.removeAllViews();

          LayoutParams tvp = new TableRow.LayoutParams(LayoutParams.WRAP_CONTENT, LayoutParams.WRAP_CONTENT, 1f);
          tvp.setMargins(1, 1, 1, 1);

          final float scale = getApplicationContext().getResources().getDisplayMetrics().density;
          int tr_height = (int)(60*scale + 0.5f);
          tvp.height = tr_height;

          TableRow header = new TableRow(this);
          //header.setBackgroundColor(ContextCompat.getColor(getApplicationContext(),R.color.alternate));
          header.setBackgroundColor(Color.BLACK);
          int color1 = ContextCompat.getColor(getApplicationContext(), R.color.alternate);

          TextView mh = new TextView(this);
          mh.setText(R.string.miles);
          //mh.setBackgroundColor(Color.WHITE);
          mh.setBackgroundColor(color1);
          mh.setTextColor(Color.BLACK);
          mh.setGravity(Gravity.CENTER);
          mh.setLayoutParams(tvp);

          TextView gh = new TextView(this);
          gh.setText(R.string.gallons);
          gh.setBackgroundColor(color1);
          gh.setTextColor(Color.BLACK);
          gh.setGravity(Gravity.CENTER);
          gh.setLayoutParams(tvp);

          TextView ch = new TextView(this);
          ch.setText(R.string.cost);
          ch.setBackgroundColor(color1);
          ch.setTextColor(Color.BLACK);
          ch.setGravity(Gravity.CENTER);
          ch.setLayoutParams(tvp);

          TextView ph = new TextView(this);
          ph.setText(R.string.mpg);
          ph.setBackgroundColor(color1);
          ph.setTextColor(Color.BLACK);
          ph.setGravity(Gravity.CENTER);
          ph.setLayoutParams(tvp);

          TextView dh = new TextView(this);
          dh.setText(R.string.date);
          dh.setBackgroundColor(color1);
          dh.setTextColor(Color.BLACK);
          dh.setGravity(Gravity.CENTER);
          dh.setLayoutParams(tvp);


          header.addView(mh);
          header.addView(gh);
          header.addView(ch);
          header.addView(ph);
          header.addView(dh);

          thetable.addView(header);


          //for(int repeat=0;repeat<15;repeat++) {
          for (int i = 0; i < values.size(); i++)
          {

               TableRow r = new TableRow(this);
               r.setBackgroundColor(Color.BLACK);

//            Log.d("showdata", values.get(i).toString());
//
//            Log.d("showdata",Double.toString(values.get(i).getMiles()));
//            Log.d("showdata get formatted",values.get(i).getMilesFormatted());

               //miles
               TextView v = new TextView(this);
               v.setText(values.get(i).getMilesFormatted());
               v.setGravity(Gravity.CENTER);
               if (i % 2 == 0)
                    v.setBackgroundColor(Color.WHITE);
               else
                    v.setBackgroundColor(color1);
               v.setLayoutParams(tvp);

               //gallons
               TextView w = new TextView(this);
               w.setText(values.get(i).getGallonsFormatted());
               w.setGravity(Gravity.CENTER);
               if (i % 2 == 0)
                    w.setBackgroundColor(Color.WHITE);
               else
                    w.setBackgroundColor(color1);
               w.setLayoutParams(tvp);

               //cost
               TextView cost = new TextView(this);
               cost.setText(values.get(i).getCostFormatted());
               cost.setGravity(Gravity.CENTER);
               if (i % 2 == 0)
                    cost.setBackgroundColor(Color.WHITE);
               else
                    cost.setBackgroundColor(color1);
               cost.setLayoutParams(tvp);

               //mpg
               TextView mpg = new TextView(this);
               mpg.setText(values.get(i).getmpgFormat());
               mpg.setGravity(Gravity.CENTER);
               if (i % 2 == 0)
                    mpg.setBackgroundColor(Color.WHITE);
               else
                    mpg.setBackgroundColor(color1);
               mpg.setLayoutParams(tvp);

               //date
               TextView date = new TextView(this);
               String fixme = values.get(i).getDate();
               String fixed = "";
               if (!fixme.equals("0000-00-00"))
               {
                    try
                    {
                         SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd", Locale.US);
                         Date nd = df.parse(fixme);
                         df = new SimpleDateFormat("MMMM d, yyyy", Locale.US);
                         if (nd != null)
                              fixed = df.format(nd);
                    }
                    catch (ParseException pe)
                    {
                         fixed = "";
                    }
               } else
               {
                    fixed = "";
               }
               date.setText(fixed);
               date.setGravity(Gravity.CENTER);
               if (i % 2 == 0)
                    date.setBackgroundColor(Color.WHITE);
               else
                    date.setBackgroundColor(color1);
               date.setLayoutParams(tvp);


               r.addView(v);
               r.addView(w);
               r.addView(cost);
               r.addView(mpg);
               r.addView(date);

               r.setOnLongClickListener(new View.OnLongClickListener()
               {
                    @Override
                    public boolean onLongClick(View v)
                    {

                         // TODO Auto-generated method stub
                         TableLayout layout = (TableLayout) v.getParent();
                         final int index = layout.indexOfChild(v);
                         if ((index - 1) <= values.size())
                         {
                              singleRecord = values.get(index - 1);
                         }

                         AlertDialog.Builder builder = new AlertDialog.Builder(ShowData.this);
                         builder.setMessage("What do you want to do with this item?");

                         //edit button
                         builder.setPositiveButton("Edit", new DialogInterface.OnClickListener()
                         {
                              @Override
                              public void onClick(DialogInterface dialog, int which)
                              {
                                   Intent intent = new Intent(getBaseContext(), EditRecord.class);
                                   //Bundle b = new Bundle();
                                   Bundle b = singleRecord.toBundle();
                                   intent.putExtras(b);
                                   startActivityForResult(intent, 2);
                              }
                         });
                         //delete button
                         builder.setNegativeButton("Delete", new DialogInterface.OnClickListener()
                         {
                              @Override
                              public void onClick(DialogInterface dialog, int which)
                              {
                                   Toast.makeText(getApplicationContext(), "really delete this mileage record?", Toast.LENGTH_SHORT).show();
                                   AlertDialog.Builder builder = new AlertDialog.Builder(ShowData.this);
                                   builder.setMessage("Are you sure?").setPositiveButton("Yes", dialogClickListener)
                                           .setNegativeButton("No", dialogClickListener).show();

                              }
                         });

                         AlertDialog alert = builder.create();
                         alert.show();

                         return true;
                    }
               });
               thetable.addView(r);
          }
     }

     private final DialogInterface.OnClickListener dialogClickListener = new DialogInterface.OnClickListener()
     {
          @Override
          public void onClick(DialogInterface dialog, int which)
          {
               switch (which)
               {
                    case DialogInterface.BUTTON_POSITIVE:
                         //Yes button clicked
                         //delete the record here
                         getDbaseHandle().deleteMileageRecord(singleRecord);
                         updateTable();
                         break;
                    case DialogInterface.BUTTON_NEGATIVE:
                         //No button clicked
                         break;
               }
          }
     };

     protected void onActivityResult(int requestCode, int ResultCode, Intent data)
     {
          super.onActivityResult(requestCode,ResultCode,data);
          vehicle = getVehicle();
          updateTable();
     }


//    public void onResume()
//    {
//        Log.d("selectvehicle","on resume needs to happen here");
//        Log.d("selectvehicle",vehicle.toString());
//        super.onResume();
//    }

     private ArrayList<Mileage> values;
     private TableLayout thetable;
     private Mileage singleRecord;
     private Vehicle vehicle;
}