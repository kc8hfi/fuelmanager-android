package org.myhouse.FuelManager;

import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.widget.Toast;



import androidx.appcompat.app.AppCompatActivity;

public abstract class Dashboard extends AppCompatActivity
//public abstract class Dashboard extends Activity
{
     protected void onCreate(Bundle savedInstanceState)
     {
          super.onCreate(savedInstanceState);
          pref = PreferenceManager.getInstance(this);
     }

     @Override
     public boolean onCreateOptionsMenu(Menu menu)
     {
          MenuInflater inflater = getMenuInflater();
          inflater.inflate(R.menu.main_menu, menu);
          return super.onCreateOptionsMenu(menu);
     }

     /*
          what to do when a menu option gets selected
      */
     @Override
     public boolean onOptionsItemSelected(MenuItem item)
     {
          Intent intent;
          if (item.getItemId() == R.id.settings)
          {
               //open up the settings menu
               intent = new Intent(getApplicationContext(), Settings.class);
               startActivity(intent);

          }
          else if (item.getItemId() == R.id.addEditVehicle)
          {
               intent = new Intent(getApplicationContext(), AddEditVehicle.class);
               //startActivity(intent);
               startActivityForResult(intent, 2);
          }
          else if (item.getItemId() == R.id.selectVehicle)
          {
               intent = new Intent(getApplicationContext(), SelectVehicle.class);
               startActivityForResult(intent, 1);
          }
//          switch (item.getItemId())
//          {
//               case R.id.settings:
//                    //open up the settings menu
//                    intent = new Intent(getApplicationContext(), Settings.class);
//                    startActivity(intent);
//                    break;
//               case R.id.addEditVehicle:
//                    intent = new Intent(getApplicationContext(), AddEditVehicle.class);
//                    //startActivity(intent);
//                    startActivityForResult(intent, 2);
//                    break;
//               case R.id.selectVehicle:
//                    intent = new Intent(getApplicationContext(), SelectVehicle.class);
//                    startActivityForResult(intent, 1);
//                    //startActivity(intent);
//                    break;
//               case R.id.testButton:
//
//                    DbaseHelper db = DbaseHelper.getInstance(this);
//                    db.getWritableDatabase().delete("vehicles",null,null);
//                    Log.d("dashboard", "testbutton, after deleting");
//               break;
//          }

          return super.onOptionsItemSelected(item);
     }

     //Inside the activity that makes a connection to the helper class
     @Override
     protected void onDestroy()
     {
          super.onDestroy();
     }

     protected void onActivityResult(int requestCode, int resultCode, Intent data)
     {
          super.onActivityResult(requestCode, resultCode, data);
     }

     public void toast(String msg)
     {
          Toast.makeText(getApplicationContext(), msg, Toast.LENGTH_LONG).show();
     } // end toast

     protected Vehicle getVehicle()
     {
          int savedVehicle = pref.getVehicleID();
          DbaseHelper dbase = DbaseHelper.getInstance(this);
          Vehicle vehicle = dbase.getVehicle(savedVehicle);
          return vehicle;
     }

     public String getHostname()
     {
          String h = pref.getHostname();
          return h;
     }

     public void setHostname(String h)
     {
          pref.setHostname(h);
     }

     public String getDatabase()
     {
          String d = pref.getDatabase();
          return d;
     }

     public void setDatabase(String d)
     {
          pref.setDatabase(d);
     }

     public int getVehicleID()
     {
          int s = pref.getVehicleID();
          return s;
     }

     public void setVehicleID(int id)
     {
          pref.setVehicleID(id);
     }

     public String getGetChangesURL()
     {
          return pref.getGetChangesURL();
     }
     public void setGetChangesURL(String u)
     {
          pref.setGetChangesURL(u);
     }

     public String getPutChangesURL()
     {
          return pref.getPutChangesURL();
     }
     public void setPutChangesURL(String s)
     {
          pref.setPutChangesURL(s);
     }

     public String getGetRefreshEverythingURL()
     {
          return pref.getGetRefreshEverythingURL();
     }
     public void setGetRefreshEverythingURL(String s)
     {
          pref.setGetRefreshEverythingURL(s);
     }

     public void setClearURL(String t)
     {
          pref.setClearURL(t);
     }
     public String getClearURL()
     {
          return pref.getClearURL();
     }

     public DbaseHelper getDbaseHandle()
     {
          return DbaseHelper.getInstance(this);
     }

     private PreferenceManager pref;
}