package org.myhouse.FuelManager;

import android.app.Activity;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import androidx.appcompat.app.AlertDialog;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.EditText;
import android.widget.ListView;

import java.util.ArrayList;

public class AddEditVehicle extends Dashboard
{
     @Override
     protected void onCreate(Bundle savedInstanceState)
     {
          super.onCreate(savedInstanceState);
          setContentView(R.layout.activity_add_edit_vehicle);

          names = new ArrayList<>();
          names.clear();
          names.addAll(getDbaseHandle().getAllVehicles());
          //names.addAll(dbase.getAllVehicles());

          adapter = new VehicleAdapter(this, names);
          adapter.notifyDataSetChanged();

          Log.d("selectvehicle", "how many vehicles: " + adapter.getCount());

          ListView lv = findViewById(R.id.vehicleList);
          lv.setAdapter(adapter);

          lv.setOnItemLongClickListener(new AdapterView.OnItemLongClickListener()
          {
               @Override
               public boolean onItemLongClick(AdapterView<?> arg0, View arg1,
                                              int pos, long id)
               {
                    //Log.d("addedit", "something should be happening here");
                    // TODO Auto-generated method stub
                    vehicle = names.get(pos);
                    AlertDialog.Builder dialog = new AlertDialog.Builder(AddEditVehicle.this);
                    dialog.setMessage("What to do with this vehicle?");
                    //positive buttion is for editing
                    dialog.setPositiveButton("Edit", new DialogInterface.OnClickListener()
                    {
                         @Override
                         public void onClick(DialogInterface dialog, int which)
                         {
                              //Log.d("addedit", "long click happened");
                              Bundle b = vehicle.toBundle();
                              Intent intent = new Intent(getApplicationContext(), EditVehicleDescription.class);
                              intent.putExtras(b);
                              startActivityForResult(intent, 1);
                         }
                    });
                    //negative button is for deleting
                    dialog.setNegativeButton("Delete", new DialogInterface.OnClickListener()
                    {
                         @Override
                         public void onClick(DialogInterface dialog, int which)
                         {
                              //Toast.makeText(getApplicationContext(), "do they really want to delete?", Toast.LENGTH_SHORT).show();
                              AlertDialog.Builder builder = new AlertDialog.Builder(AddEditVehicle.this);
                              builder.setMessage("Are you sure?").setPositiveButton("Yes", dialogClickListener)
                                      .setNegativeButton("No", dialogClickListener).show();

                         }
                    });


                    AlertDialog alert = dialog.create();
                    alert.show();

                    return true;
               }
          });
     }//end onCreate


     private final DialogInterface.OnClickListener dialogClickListener = new DialogInterface.OnClickListener()
     {
          @Override
          public void onClick(DialogInterface dialog, int which)
          {
               switch (which)
               {
                    case DialogInterface.BUTTON_POSITIVE:
                         //Yes button clicked
                         getDbaseHandle().deleteVehicle(vehicle);
                         refreshVehicleList();
                    break;
                    case DialogInterface.BUTTON_NEGATIVE:
                         //No button clicked
                    break;
               }
          }
     };

     protected void onActivityResult(int requestCode, int ResultCode, Intent data)
     {
          super.onActivityResult(requestCode, ResultCode, data);
          if (requestCode == 1)
          {
               //Activity.RESULT_OK = -1,  Activity.RESULT_CANCELED = 0
               if (ResultCode == Activity.RESULT_CANCELED)
               {
                    refreshVehicleList();
               }
          }
     }


     public void save(View v)
     {
          //Log.d("save", "save the new vehicle if possible");
          EditText desc = findViewById(R.id.newDesc);
          String d = desc.getText().toString();
          if (!d.equals(""))
          {
               long returnVal = getDbaseHandle().addVehicle(desc.getText().toString());
               if (returnVal > 0)
               {
                    desc.setText("");
                    refreshVehicleList();
               }
          }
     }

     private void refreshVehicleList()
     {
          names.clear();
          names.addAll(getDbaseHandle().getAllVehicles());
          adapter.notifyDataSetChanged();
     }

     private Vehicle vehicle;
     private ArrayList<Vehicle> names;
     private VehicleAdapter adapter;
}