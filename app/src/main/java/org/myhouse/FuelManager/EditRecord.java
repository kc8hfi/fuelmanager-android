package org.myhouse.FuelManager;

import android.content.Intent;
import android.os.Bundle;
import androidx.annotation.NonNull;
import android.view.View;
import android.widget.CalendarView;
import android.widget.EditText;
import android.widget.Toast;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

public class EditRecord extends Dashboard //implements DatePickerDialog.OnDateSetListener
{
    /**
     * onCreate - called when activity is first created
     */
    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_edit_record);

        //get the item from the intent
        Intent intent = getIntent();
        Bundle b = intent.getExtras();

        //make an item from the bundle of extra stuff
        if (b != null)
        {
            item = new Mileage(b);

            miles = findViewById(R.id.miles);
            //miles.setText(Double.toString(item.getMiles()));
            miles.setText(item.getMilesFormatted());


            gallons = findViewById(R.id.gallons);
            //gallons.setText(Double.toString(item.getGallons()));
            gallons.setText(item.getGallonsFormatted());

            cost = findViewById(R.id.cost);
            //cost.setText(Double.toString(item.getCost()));
            cost.setText(item.getCostFormatted());


            //the date looks like YYYY-MM-DD
            //turn this into the number of milliseconds since jan 1 1970
            calendarView = findViewById(R.id.date);
            try
            {
                SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd", Locale.US);
                Date newdate = sdf.parse(item.getDate());
                calendarView.setDate(newdate.getTime());
            }
            catch (ParseException pe)
            {

            }
            //sets up a listener so when they change the date, set it correctly
            calendarView.setOnDateChangeListener(new CalendarView.OnDateChangeListener()
            {
                @Override
                public void onSelectedDayChange(@NonNull CalendarView view, int year, int month, int dayOfMonth)
                {
                    String msg = year + "-" + (month + 1) + "-" + dayOfMonth;
                    //Toast.makeText(getApplicationContext(),msg,Toast.LENGTH_LONG).show();

                    try
                    {
                        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-M-dd", Locale.US);
                        Date newdate = sdf.parse(msg);
                        calendarView.setDate(newdate.getTime());
                    }
                    catch (ParseException pe)
                    {

                    }
                }
            });
        }
    }//end onCreate

    /**
     * Save their changes to the database
     */
    public void saveChanges(View v)
    {
        Vehicle vehicle = getVehicle();

        //String date = formatInsertDate(dateButton.getText().toString());
        String message = "Please Fill out:\n";
        int ok = 0;

        if (miles.getText().toString().equals(""))
        {
            message += "miles\n";
            ok = 1;
        }
        if (gallons.getText().toString().equals(""))
        {
            message += "gallons\n";
            ok = 1;
        }
        if (cost.getText().toString().equals(""))
        {
            message += "cost\n";
            ok = 1;
        }
        if (ok == 1)
        {
            Toast.makeText(getApplicationContext(),message,Toast.LENGTH_LONG).show();
        }
        else //everything is ok, update the record
        {
            //fix the Mileage item
            item.setMiles(Double.parseDouble(miles.getText().toString()));
            item.setGallons(Double.parseDouble(gallons.getText().toString()));

            //this is the formatted string, with $ and stuff, can't just call parseDouble on
            //item.setCost(Double.parseDouble(cost.getText().toString()));

            String c = cost.getText().toString().replaceAll("[^\\d.]","");
            item.setCost(Double.parseDouble(c));


            if (Double.parseDouble(gallons.getText().toString()) != 0)
            {
                Double t = Double.parseDouble(miles.getText().toString()) / Double.parseDouble(gallons.getText().toString());
                //item.setMpg(String.format("%.2f",Double.toString(t)));
                item.setMpg(t);
            }
            else
            {
                item.setMpg(0.0);
            }

            String formattedDate;

            SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd",Locale.US);
            long number = calendarView.getDate();
            Date newdate = new Date(number);
            formattedDate = sdf.format(newdate);
            item.setDate(formattedDate);

            //Toast.makeText(getApplicationContext(),item.toString(),Toast.LENGTH_LONG).show();

            long returnVal = getDbaseHandle().updateRecord(item,vehicle);
            if (returnVal != -1)
            {
                Intent result = new Intent();
                Bundle b;
                b = item.toBundle();
                result.putExtras(b);
                setResult(RESULT_OK,result);
            }
        }
        finish();
    }

    /**
     * don't do anything, just close
     */
    public void cancelChanges(View v)
    {
        setResult(RESULT_CANCELED);
        finish();
    }

    private Mileage item;
    private EditText miles;
    private EditText gallons;
    private EditText cost;
    private CalendarView calendarView;

}//end class EditRecord