package org.myhouse.FuelManager;

public class DataObject
{
     private String whichOne;
     private StringBuilder theData;

     public DataObject()
     {
          whichOne = "";
          theData = new StringBuilder();
     }

     public String getWhichOne()
     {
          return whichOne;
     }

     public StringBuilder getTheData()
     {
          return theData;
     }

     public void setWhichOne(String s)
     {
          whichOne = s;
     }

     public void setTheData(StringBuilder b)
     {
          theData = b;
     }
}