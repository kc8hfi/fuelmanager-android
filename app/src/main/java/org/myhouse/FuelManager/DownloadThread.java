package org.myhouse.FuelManager;

import android.content.ContentValues;
import android.os.Handler;
import android.os.HandlerThread;
import android.os.Message;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.net.URL;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;

import javax.net.ssl.HttpsURLConnection;

public class DownloadThread extends HandlerThread
{

     private Handler mWorkerHandler;
     private Handler mResponseHandler;
     private DownloadThread.Callback mCallback;
     private static final String TAG = DownloadThread.class.getSimpleName();

     private String hostname;
     private String database;
     private String username;
     private String password;
     private String url;

     private ArrayList<DataObject> everything;


     public DownloadThread(Handler responseHandler, DownloadThread.Callback callback)
     {
          super(TAG);
          mResponseHandler = responseHandler;
          mCallback = callback;

          everything = new ArrayList<DataObject>();
     }

     public interface Callback
     {
          public void downloadEverythingHandler(int responsecode, ArrayList<DataObject>  data);
     }


     public void prepareHandler()
     {
          mWorkerHandler = new Handler(getLooper(), new Handler.Callback()
          {
               public boolean handleMessage(Message msg)
               {
                    handleRequest();
                    return true;
               }
          });
     }


     public void queueTask(String host, String db, String u, String p, String t)
     {
          hostname = host;
          database = db;
          username = u;
          password = p;
          url = t;

//          //Log.d(TAG, "set up all the connection stuff");
//          //Log.d(TAG,"url: " + url);
          mWorkerHandler.obtainMessage(1).sendToTarget();
     }

     private void handleRequest()
     {
//          Log.d(TAG,"the vehicles part of sync is running " + urls.size());
//          Log.d(TAG,"url: " + urls.get(0));
//          for(int i=0;i<urls.size();i++)
//          {
               //get vehicle changes
               DataObject o = new DataObject();
               o.setWhichOne("vehicles");
               o.setTheData(doWork(url,"vehicles"));
               everything.add(o);

               //get mileage changes
               o = new DataObject();
               o.setWhichOne("mileage");
               o.setTheData(doWork(url,"mileage"));
               everything.add(o);

               // these 2 need to be fixed!!
               //put vehicle changes
//               o = new SyncDataObject();
//               o.setWhichOne(urls.get(i));
//               o.setTheData(doWork(urls.get(i),"put_vehicles"));
//               everything.add(o);
//
//               //put mileage changes
//               o = new SyncDataObject();
//               o.setWhichOne(urls.get(i));
//               o.setTheData(doWork(urls.get(i),"put_mileage"));
//               everything.add(o);

//          }
//          getVehicles();
//          getMileage();
//          final int responseCode = 200;
//          mResponseHandler.post(new Runnable()
//          {
//               @Override
//               public void run()
//               {
//                    mCallback.refreshEverythingHandler(responseCode, everything);
//               }
//          });
          mResponseHandler.post(new Runnable()
          {
               @Override
               public void run()
               {
                    mCallback.downloadEverythingHandler(1,everything);
               }
          });

     }

     private StringBuilder doWork(String u, String whichOne)
     {
          String webpage = "https://" + hostname + u;
          //everything.setWebpage(webpage);
          final StringBuilder result = new StringBuilder();
          try
          {
               URL url = new URL(webpage);
               HttpsURLConnection httpclient = (HttpsURLConnection) url.openConnection();
               httpclient.setRequestMethod("POST");

               ContentValues credentials = new ContentValues();
               credentials.put("username", username);
               credentials.put("password", password);
               credentials.put("database", database);
               //set up the request to get the vehicles
               credentials.put("clickme", whichOne);

               ArrayList<StringBuilder> stuff = new ArrayList<>();
               for(String key:credentials.keySet())
               {
                    StringBuilder t = new StringBuilder();
                    t.append(key);
                    t.append("=");
                    t.append(credentials.get(key));
                    stuff.add(t);
               }

               String sendme = String.join("&",stuff);


               OutputStream os = httpclient.getOutputStream();
               BufferedWriter writer = new BufferedWriter(new OutputStreamWriter(os, StandardCharsets.UTF_8));
               writer.write(sendme);
               writer.flush();
               writer.close();
               os.close();

               final int responseCode = httpclient.getResponseCode();
               if (responseCode == HttpsURLConnection.HTTP_OK)
               {
                    String line;
                    BufferedReader br = new BufferedReader(new InputStreamReader(httpclient.getInputStream()));
                    while ((line = br.readLine()) != null)
                    {
                         result.append(line);
                    }
               }
          } catch (
                  IOException e)
          {
               e.printStackTrace();
          }
          return result;
     }




}
