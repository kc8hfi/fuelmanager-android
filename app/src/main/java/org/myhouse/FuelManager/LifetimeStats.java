package org.myhouse.FuelManager;

import android.content.Intent;
import android.os.Bundle;
import android.util.Base64;
import android.webkit.WebView;
import android.widget.Toast;

import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.Locale;

public class LifetimeStats extends Dashboard {

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_lifetime_stats);

        view = findViewById(R.id.webView);

//        fillups = findViewById(R.id.fillups);
//        miles = findViewById(R.id.miles);
//        gallons = findViewById(R.id.gallons);
//        mpg = findViewById(R.id.mpg);
//        cost = findViewById(R.id.cost);

        vehicle = getVehicle();
        if (vehicle.getVehicleId() != -1)
        {
            updateStats();
        }
        else
        {
            Toast.makeText(getApplicationContext(),"select a vehicle first!",Toast.LENGTH_LONG).show();
        }
    }

    private void updateStats()
    {
        //int color1 = ContextCompat.getColor(getApplicationContext(),R.color.alternate);

        //int color1 = getApplicationContext().getResources().getColor(R.color.alternate);
        DbaseHelper dbase = getDbaseHandle();
        ArrayList<String> data = dbase.getLifetimeStats(vehicle);

        StringBuilder thepage = new StringBuilder();
        thepage.append(
                "<!DOCTYPE HTML>\n" +
                        "<html>\n" +
                        "<title>sample</title>\n" +
                        "<style type=\"text/css\">\n" +
                        ".bgcolor\n" +
                        "{\n" +
                        "/*      background-color: #D8D8D8; */\n" +
                        "     background-color:   #c0dbea;\n" +
                        "}\n" +
                        "\n" +
                        "/* fix up the look of tables */\n" +
                        "/* table */\n" +
                        ".tabledata\n" +
                        "{\n" +
                        "     \n" +
                        "     /* show empty cells as regular cells */\n" +
                        "     empty-cells:   show;\n" +
                        "     border-width:  1px;\n" +
                        "     border-spacing:     2px;\n" +
                        "     border-collapse:    collapse;\n" +
                        "}\n" +
                        "\n" +
                        "/* table td,th  */\n" +
                        ".tabledata td,th\n" +
                        "{\n" +
                        "     /* make sure the table td fields don't wrap if they are long */\n" +
                        "     white-space: nowrap;\n" +
                        "     \n" +
                        "     border-width:  1px;\n" +
                        "     padding:  10px;\n" +
                        "     border-style:  solid;\n" +
                        "}\n" +
                        "\n" +
                        "</style>\n" +
                        "<body>\n" +
                        "\n" +
                        "<table class=\"tabledata\" border=\"1\"> \n");

        thepage.append("<tr class=\"bgcolor\">");
        thepage.append("<td>Fillups</td>");
        thepage.append("<td>" + data.get(0) + "</td>");
        thepage.append("</tr>");

        thepage.append("<tr>");
        thepage.append("<td>Miles</td>");
        thepage.append("<td>" + data.get(1) + "</td>");
        thepage.append("</tr>");

        thepage.append("<tr class=\"bgcolor\">");
        thepage.append("<td>Gallons</td>");
        thepage.append("<td>" + data.get(2) + "</td>");
        thepage.append("</tr>");

        thepage.append("<tr>");
        thepage.append("<td>MPG</td>");
        thepage.append("<td>" + data.get(3) + "</td>");
        thepage.append("</tr>");

        thepage.append("<tr class=\"bgcolor\">");
        thepage.append("<td>Cost</td>");
        NumberFormat nf = NumberFormat.getCurrencyInstance(Locale.US);
        String fixed = nf.format(Double.parseDouble(data.get(4)));

        thepage.append("<td>" + fixed + "</td>");
        thepage.append("</tr>");

        thepage.append("</table>\n").append("</body>\n").append("</html>\n");

        //clear the page first
        view.loadUrl("about:blank");
        //put the page onto the webview
        String encodeMe = Base64.encodeToString(thepage.toString().getBytes(), Base64.NO_PADDING);
        view.loadData(encodeMe, "text/html", "base64");





//        int color1 = ContextCompat.getColor(getApplicationContext(),R.color.alternate);
//
//        TableRow.LayoutParams tvp = new TableRow.LayoutParams(TableRow.LayoutParams.WRAP_CONTENT, TableRow.LayoutParams.WRAP_CONTENT,1f);
//        tvp.setMargins(1,1,1,1);
//
//        TableRow fillupRow = findViewById(R.id.fillupRow);
//        fillupRow.setBackgroundColor(Color.BLACK);
//        findViewById(R.id.fillupsText).setLayoutParams(tvp);
//        findViewById(R.id.fillupsText).setBackgroundColor(color1);
//        fillups.setText(data.get(0));
//        fillups.setBackgroundColor(color1);
//        fillups.setLayoutParams(tvp);
//
//        TableRow milesRow = findViewById(R.id.milesRow);
//        milesRow.setBackgroundColor(Color.BLACK);
//        findViewById(R.id.milesText).setLayoutParams(tvp);
//        findViewById(R.id.milesText).setBackgroundColor(Color.WHITE);
//        miles.setText(data.get(1));
//        miles.setBackgroundColor(Color.WHITE);
//        miles.setLayoutParams(tvp);
//
//        TableRow gallonsRow = findViewById(R.id.gallonsRow);
//        gallonsRow.setBackgroundColor(Color.BLACK);
//        findViewById(R.id.gallonsText).setLayoutParams(tvp);
//        findViewById(R.id.gallonsText).setBackgroundColor(color1);
//        gallons.setText(data.get(2));
//        gallons.setBackgroundColor(color1);
//        gallons.setLayoutParams(tvp);
//
//        TableRow mpgRow = findViewById(R.id.mpgRow);
//        mpgRow.setBackgroundColor(Color.BLACK);
//        findViewById(R.id.mpgText).setLayoutParams(tvp);
//        findViewById(R.id.mpgText).setBackgroundColor(Color.WHITE);
//        mpg.setText(data.get(3));
//        mpg.setBackgroundColor(Color.WHITE);
//        mpg.setLayoutParams(tvp);
//
//        TableRow costRow = findViewById(R.id.costRow);
//        costRow.setBackgroundColor(Color.BLACK);
//        findViewById(R.id.costText).setLayoutParams(tvp);
//        findViewById(R.id.costText).setBackgroundColor(color1);
//
//        NumberFormat nf = NumberFormat.getCurrencyInstance(Locale.US);
//        String fixed = nf.format(Double.parseDouble(data.get(4)));
//
//        cost.setText(fixed);
//        cost.setBackgroundColor(color1);
//        cost.setLayoutParams(tvp);
    }

    @Override
    protected void onActivityResult(int requestCode, int ResultCode, Intent data)
    {
        super.onActivityResult(requestCode, ResultCode, data);
        vehicle = getVehicle();
        updateStats();
    }


    //private ArrayList<String> data;

//    private TextView fillups;
//    private TextView miles;
//    private TextView gallons;
//    private TextView mpg;
//    private TextView cost;
    private Vehicle vehicle;

    private WebView view;


}