package org.myhouse.FuelManager;

import android.app.Activity;
import android.os.Bundle;
import android.view.Gravity;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.TextView;

import java.util.ArrayList;

public class SelectVehicle extends Dashboard
{
     @Override
     protected void onCreate(Bundle savedInstanceState)
     {
          super.onCreate(savedInstanceState);
          setContentView(R.layout.activity_select_vehicle);

          Vehicle current = getVehicle();
          if (current.getVehicleId() != -1)
          {
               TextView t = findViewById(R.id.textView2);
               t.setText(current.getVehicleDescription());

               final float scale = getApplicationContext().getResources().getDisplayMetrics().density;
               int tr_height = (int)(60*scale + 0.5f);
               t.setMinHeight(tr_height);
               t.setGravity(Gravity.CENTER);
               //t.setTextSize(24);
          }
          names = new ArrayList<>();
          VehicleAdapter adapter = new VehicleAdapter(this, names);
          names.clear();

          ArrayList<Vehicle> biglist = getDbaseHandle().getAllVehicles();
          if (biglist.size() == 0)
          {
               toast("Add some vehicles first!");
          }
          else
          {
               names.addAll(biglist);
          }

          adapter.notifyDataSetChanged();

          ListView lv = findViewById(R.id.vehicleList);
          lv.setAdapter(adapter);

          lv.setOnItemClickListener(new AdapterView.OnItemClickListener()
          {
               public void onItemClick(AdapterView<?> parent, View view, int position, long id)
               {
               //Log.d("selectvehicle",names.get(position).toString());
               //pref.setVehicleID(names.get(position).getVehicleId());
               setVehicleID(names.get(position).getVehicleId());
               //pref.setVehicleLocation(names.get(position).getVehicleLocation());

               //Activity.RESULT_CANCELED = 0,  Activity.RESULT_OK = -1
               setResult(Activity.RESULT_CANCELED);
               finish();
               }
          });
     }

     //private PreferenceManager pref;
     private ArrayList<Vehicle> names;
}