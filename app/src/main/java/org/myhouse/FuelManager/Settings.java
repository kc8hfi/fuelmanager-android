package org.myhouse.FuelManager;

import android.content.DialogInterface;
import android.os.Bundle;

import android.os.Handler;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;

import androidx.appcompat.app.AlertDialog;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

public class Settings extends Dashboard implements
               RefreshEverythingThread.Callback, DownloadThread.Callback,
               UploadThread.Callback,ClearChangesThread.Callback

{

     private EditText host;
     private EditText db;
     private EditText user;
     private EditText pass;

     String username;
     String password;

     //for refreshing the entire database
     private RefreshEverythingThread mSyncRefreshEverything;

     //for pulling down the changes from the server
     private DownloadThread mDownloadEverything;

     //for uploading all the changes to the server
     private UploadThread mUploadEverything;

     //for telling the server to clear out its changes tables
     private ClearChangesThread mClearChanges;

     @Override
     protected void onCreate(Bundle savedInstanceState)
     {
          super.onCreate(savedInstanceState);
          setContentView(R.layout.activity_settings);

          //pull the hostname and dbase from the preferences
          ((EditText) findViewById(R.id.hostname)).setText(getHostname());
          ((EditText) findViewById(R.id.database)).setText(getDatabase());

          host = findViewById(R.id.hostname);
          db = findViewById(R.id.database);
          user = findViewById(R.id.username);
          pass = findViewById(R.id.password);

     }

     //     @Override
     public boolean onOptionsItemSelected(MenuItem item)
     {
          //Log.d("settings","in the onoptionsitemselected, before returning call to super");
          return super.onOptionsItemSelected(item);
     }

     public void saveSettings(View view)
     {
          //Log.d("settings", "save settings");
          //EditText hostname = findViewById(R.id.hostname);
          //EditText database = findViewById(R.id.database);
          int ok = 1;
          if (host.getText().toString().equals(""))
          {
               ok = 0;
          }
          if (db.getText().toString().equals(""))
          {
               ok = 0;
          }
          if (ok != 0)
          {
               setHostname(host.getText().toString());
               setDatabase(db.getText().toString());

               setGetChangesURL("/connector/get_changes.php");
               setPutChangesURL("/connector/put_changes.php");
               setGetRefreshEverythingURL("/connector/get_refresh_everything.php");
               setClearURL("/connector/clear_tables.php");

          }
     }//end saveSettings


     //gets called with the sync button gets pressed
     public void syncEverything (View view)
     {
          String hostname = "";
          String database = "";

          //did
          // they type in a different hostname?  if so, use it instead
          if (!host.getText().toString().equals(getHostname()))
          {
               hostname = host.getText().toString();
          } else
          {
               hostname = getHostname();
          }
          //did they type in a different database?  if so, use it instead
          if (!db.getText().toString().equals(getDatabase()))
          {
               database = db.getText().toString();
          } else
          {
               database = getDatabase();
          }

          username = user.getText().toString().trim();
          password = pass.getText().toString().trim();

          //make sure they typed in a username and password, and there is a hostname and database
          if (!username.equals("") && !password.equals("")
                  && !hostname.equals("") && !database.equals(""))
          {
               //pull down any changes from the server first
               mDownloadEverything = new DownloadThread(new Handler(), this);
               mDownloadEverything.start();
               mDownloadEverything.prepareHandler();
               mDownloadEverything.queueTask(hostname,database,username,password,getGetChangesURL());
          }
     }//end syncEverything

     @Override
     public void downloadEverythingHandler(int whichOne, ArrayList<DataObject> data)
     {
          Log.d("downloadEverythingHandler","how many: " +  data.size());

          boolean syncV = false;
          boolean syncFM = false;
          DbaseHelper db = DbaseHelper.getInstance(this);
          for(int i=0;i<data.size();i++)
          {
               DataObject t = data.get(i);
               if (t.getWhichOne().equals("vehicles"))
               {
                    try
                    {
                         Log.d("downloadeverythinghandler","incoming vehicles: "+ t.getTheData().toString());
                         StringBuilder s = t.getTheData();

                         JSONObject obj = new JSONObject(s.toString());
                         JSONArray arr = obj.getJSONArray("vehicles");
                         syncV = db.syncVehicles(arr);
                    }
                    catch(JSONException e)
                    {
                         e.printStackTrace();
                    }
               }
               else if (t.getWhichOne().equals("mileage"))
               {
                    try
                    {
                         StringBuilder s = t.getTheData();
                         Log.d("downloadeverythinghandler","incoming fm: "+ t.getTheData().toString());
                         JSONObject obj = new JSONObject(s.toString());
                         JSONArray arr = obj.getJSONArray("fuel_mileage");
                         syncFM = db.syncFuelMileage(arr);
                    }
                    catch(JSONException e)
                    {
                         e.printStackTrace();
                    }
               }
          }
          //invalidate and then rebuild the views



          //make sure both vehicles and fuel mileage that came down from the server
          //were inserted/updated first, then send up the local stuff
          if (syncV && syncFM)
          {
               Log.d("downloadEverythingHandler", "start another thread to tell the server it can clean out its changes tables");
               startCleanupServerThread();
          }
          else
          {
               Log.d("downloadEverythingHandler","either syncV or syncFM was false");
          }
     }





     private void startCleanupServerThread()
     {
          String hostname = "";
          String database = "";

          //did they type in a different hostname?  if so, use it instead
          if (!host.getText().toString().equals(getHostname()))
          {
               hostname = host.getText().toString();
          } else
          {
               hostname = getHostname();
          }
          //did they type in a different database?  if so, use it instead
          if (!db.getText().toString().equals(getDatabase()))
          {
               database = db.getText().toString();
          } else
          {
               database = getDatabase();
          }

          username = user.getText().toString().trim();
          password = pass.getText().toString().trim();

          //make sure they typed in a username and password, and there is a hostname and database
          if (!username.equals("") && !password.equals("")
                  && !hostname.equals("") && !database.equals(""))
          {
               mClearChanges = new ClearChangesThread(new Handler(), this);
               mClearChanges.start();
               mClearChanges.prepareHandler();

               Log.d("startCleanupServerThread","url: " + getClearURL());
               mClearChanges.queueTask(hostname,database,username,password,getClearURL());
          }
     }




     @Override
     public void clearChangesHandler(int responsecode)
     {
          //it doesn't matter if the server failed to clean its changes tables
          //we'll just try to clean them up again later
          Log.d("clearChangesHandler","after the server is cleaned, send up the local changes");
          startUploadThread();
     }




     private void startUploadThread()
     {
          DbaseHelper dbaseHelper = DbaseHelper.getInstance(this);
          JSONArray vehicleChanges = dbaseHelper.getVehicleChanges();
          JSONArray mileageChanges = dbaseHelper.getFuelMileageChanges();
          String hostname = "";
          String database = "";

          //did they type in a different hostname?  if so, use it instead
          if (!host.getText().toString().equals(getHostname()))
          {
               hostname = host.getText().toString();
          } else
          {
               hostname = getHostname();
          }
          //did they type in a different database?  if so, use it instead
          if (!db.getText().toString().equals(getDatabase()))
          {
               database = db.getText().toString();
          } else
          {
               database = getDatabase();
          }

          username = user.getText().toString().trim();
          password = pass.getText().toString().trim();

          //make sure they typed in a username and password, and there is a hostname and database
          if (!username.equals("") && !password.equals("")
                  && !hostname.equals("") && !database.equals(""))
          {
               ArrayList<DataObject> stuff = new ArrayList<>();

               DataObject item = new DataObject();
               item.setWhichOne("vehicles");
               try
               {
                    JSONObject obj = new JSONObject();
                    obj.put("vehicles",vehicleChanges);
                    StringBuilder t = new StringBuilder();
                    t.append(obj.toString());
                    item.setTheData(t);
               }
               catch(JSONException e)
               {
                    Log.d("json vehicles",e.toString());
               }


               DataObject m = new DataObject();
               m.setWhichOne("mileage");
               try
               {
                    JSONObject obj = new JSONObject();
                    obj.put("fuel_mileage",mileageChanges);
                    StringBuilder t = new StringBuilder();
                    t.append(obj.toString());
                    m.setTheData(t);
               }
               catch(JSONException e)
               {
                    Log.d("json fuel_mileage",e.toString());
               }
               stuff.add(item);
               stuff.add(m);

               mUploadEverything = new UploadThread(new Handler(), this);
               mUploadEverything.start();
               mUploadEverything.prepareHandler();
               Log.d("startUploadThread","url: " + getPutChangesURL());
               mUploadEverything.queueTask(hostname,database,username,password,stuff,getPutChangesURL());
          }
     }


     @Override
     public void uploadEverythingHandler(int responsecode, StringBuilder status)
     {
          toast("after uploading data:" + status.toString());

          Log.d("uploadeverythinghandler", status.toString());
          if (status.toString().equals("successsuccess"))
          {
               Log.d("uploadeverythinghandler", "if it was success empty the local update tables");
               DbaseHelper db = DbaseHelper.getInstance(this);
               db.deleteVehicleChangesTable();
               db.deleteFuelMileageChangesTable();
          }
     }




     //the refresh button action handler
     public void refresh(View v)
     {
          AlertDialog.Builder dialog = new AlertDialog.Builder(Settings.this);
          dialog.setMessage("Do you really want to delete all local vehicle and fuel mileage data?");
          //positive button is for yes
          dialog.setPositiveButton("Yes", new DialogInterface.OnClickListener()
          {
               @Override
               public void onClick(DialogInterface dialog, int which)
               {
                    //Log.d("refresh", "go ahead and delete everything and then pull everything down");1
                    //Log.d("refresh","here the url is: " + getGetRefreshEverythingURL());
                    refreshEverything();
               }
          });
          //negative button is for no
          dialog.setNegativeButton("No!", new DialogInterface.OnClickListener()
          {
               @Override
               public void onClick(DialogInterface dialog, int which)
               {
                    //they canceled the dialog box
               }
          });
          AlertDialog alert = dialog.create();
          alert.show();
     }

     @Override
     //callback after the sync everything thread got finished
     //public void refreshEverything(int responsecode, String data)
     public void refreshEverythingHandler(int responsecode, ArrayList<DataObject> data)
     {
          //toast("refreshEverything:" + data.getWebpage());1

          //Log.d("refreshEverything part","data:" + data.getWebpage());
          //Log.d("refreshEverything part","data:" + data.toString());
          Log.d("refresh","need to delete all local data and add stuff");

          DbaseHelper db = DbaseHelper.getInstance(this);

          //this should empty out both the vehicles and fuel_mileage tables
          //on delete cascade nukes the fuel_mileage table
          db.deleteAllLocalVehicles();

          Log.d("refresh",getGetRefreshEverythingURL());

          try
          {
               //find the vehicles data first
               int index = -1;
               for(int i=0;i<data.size();i++)
               {
                    if (data.get(i).getWhichOne().equals("vehicles"))
                    {
                         index = i;
                         break;
                    }
               }
               //get the vehicle data first
               StringBuilder v = data.get(index).getTheData();
               JSONObject obj = new JSONObject(v.toString());
               JSONArray arr = obj.getJSONArray("vehicles");
               //add the vehicle stuff furst
               db.addAllVehicles(arr);

               index = -1;
               for(int i=0;i<data.size();i++)
               {
                    if (data.get(i).getWhichOne().equals("fuel_mileage"))
                    {
                         index = i;
                         break;
                    }
               }
               //get the fuel mileage data now
               v = data.get(index).getTheData();
               obj = new JSONObject(v.toString());
               arr = obj.getJSONArray("fuel_mileage");
               //load the fuel mileage stuff
               db.addAllFuelMileage(arr);

               //now update all the view stuff
               //@here
          } catch (JSONException e)
          {
               e.printStackTrace();
          }
     }

     private void refreshEverything()
     {
          String hostname = "";
          String database = "";

          //did they type in a different hostname?  if so, use it instead
          if (!host.getText().toString().equals(getHostname()))
          {
               hostname = host.getText().toString();
          }
          else
          {
               hostname = getHostname();
          }
          //did they type in a different database?  if so, use it instead
          if (!db.getText().toString().equals(getDatabase()))
          {
               database = db.getText().toString();
          }
          else
          {
               database = getDatabase();
          }

          username = user.getText().toString().trim();
          password = pass.getText().toString().trim();

          //make sure they typed in a username and password, and there is a hostname and database
          if (!username.equals("") && !password.equals("")
                  && !hostname.equals("") && !database.equals(""))
          {
               mSyncRefreshEverything = new RefreshEverythingThread(new Handler(), this);
               mSyncRefreshEverything.start();
               mSyncRefreshEverything.prepareHandler();
               String x = getGetRefreshEverythingURL();
               mSyncRefreshEverything.queueTask(hostname,database,username,password,x);
          }
     }




     public void testButton(View view)
     {



     }

}

