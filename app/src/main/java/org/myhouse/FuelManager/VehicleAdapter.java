package org.myhouse.FuelManager;

import android.app.Activity;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import java.util.ArrayList;

class VehicleAdapter extends BaseAdapter {
    public VehicleAdapter(Activity context, ArrayList<Vehicle> names)
    {
        this.context = context;
        this.names = names;
    }

    @Override
    public long getItemId(int position)
    {
        return position;
    }

    @Override
    public Object getItem(int position)
    {

        return names.get(position);
    }

    @Override
    public int getCount()
    {
        return (names == null) ? 0 : names.size();
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent)
    {
        //View rowView = convertView;
        //ImageView image = null;
        //TextView text = null;

        if (convertView == null)
        {
            convertView = LayoutInflater.from(context).inflate(R.layout.layout_list_view,parent,false);
        }
        //get the current item to be displayed
        Vehicle v = names.get(position);

        //text view for the id and the description
        //TextView id = (TextView)convertView.findViewById(R.id.text_view_item_name);
        TextView desc = convertView.findViewById(R.id.text_view_item_description);

         final float scale = context.getResources().getDisplayMetrics().density;
         int tr_height = (int)(60*scale + 0.5f);
         desc.setMinHeight(tr_height);
         desc.setGravity(Gravity.CENTER);


         if ((position %2) ==0)
        {
            convertView.setBackgroundResource(R.color.alternate);
        }
        else
        {
            convertView.setBackgroundResource(R.color.white);
        }
        if(convertView == context.getCurrentFocus())
        {
            convertView.setBackgroundResource(R.color.red);
        }

        //set the text for the id and description
        //id.setText(Integer.toString(v.getVehicleId()));
        desc.setText(v.getVehicleDescription());




        return convertView;

    }

    private final Activity context;
    private final ArrayList<Vehicle> names;
}