package org.myhouse.FuelManager;

import android.content.Context;
import android.content.SharedPreferences;

public class PreferenceManager
{
     private static final String APP_PREFERENCES = "fuelmanager_settings";

     private static final String HOSTNAME = "hostname";
     private static final String DATABASE = "database";

     private static final String VEHICLE_ID = "vehicle_id";

     private static final String GET_CHANGES = "get_changes_url";
     private static final String PUT_CHANGES = "put_changes_url";
     private static final String GET_REFRESH_EVERYTHING = "get_refresh_everything_url";
     private static final String CLEAR_URL = "clear_url";


     private final SharedPreferences sharedprefs;
     private static PreferenceManager instance;

     private PreferenceManager(Context c)
     {
          sharedprefs = c.getApplicationContext().getSharedPreferences(APP_PREFERENCES, Context.MODE_PRIVATE);
     }

     public static synchronized PreferenceManager getInstance(Context c)
     {
          if (instance == null)
          {
               instance = new PreferenceManager(c);
          }
          return instance;
     }

     public String getHostname()
     {
          return sharedprefs.getString(HOSTNAME, "");
          //return h;
     }

     public void setHostname(String h)
     {
          SharedPreferences.Editor editor = sharedprefs.edit();
          editor.putString(HOSTNAME, h);
          editor.apply();
     }

     public String getDatabase()
     {
          return sharedprefs.getString(DATABASE, "");
     }

     public void setDatabase(String d)
     {
          SharedPreferences.Editor editor = sharedprefs.edit();
          editor.putString(DATABASE, d);
          editor.apply();
     }


     public int getVehicleID()
     {
          return sharedprefs.getInt(VEHICLE_ID, -1);
     }

     public void setVehicleID(int d)
     {
          SharedPreferences.Editor editor = sharedprefs.edit();
          editor.putInt(VEHICLE_ID, d);
          editor.apply();
     }

     public String getGetChangesURL()
     {
          return sharedprefs.getString(GET_CHANGES, "");
     }

     public void setGetChangesURL(String t)
     {
          SharedPreferences.Editor editor = sharedprefs.edit();
          editor.putString(GET_CHANGES, t);
          editor.apply();
     }

     public String getPutChangesURL()
     {
          return sharedprefs.getString(PUT_CHANGES,"");
     }

     public void setPutChangesURL(String t)
     {
          SharedPreferences.Editor editor = sharedprefs.edit();
          editor.putString(PUT_CHANGES, t);
          editor.apply();
     }

     public void setGetRefreshEverythingURL(String t)
     {
          SharedPreferences.Editor editor = sharedprefs.edit();
          editor.putString(GET_REFRESH_EVERYTHING,t);
          editor.apply();
     }

     public String getGetRefreshEverythingURL()
     {
          String x = sharedprefs.getString(GET_REFRESH_EVERYTHING,"");

          return sharedprefs.getString(GET_REFRESH_EVERYTHING,"");
     }

     public String getClearURL()
     {
          return sharedprefs.getString(CLEAR_URL, "");
     }

     public void setClearURL(String t)
     {
          SharedPreferences.Editor editor = sharedprefs.edit();
          editor.putString(CLEAR_URL, t);
          editor.apply();
     }
}