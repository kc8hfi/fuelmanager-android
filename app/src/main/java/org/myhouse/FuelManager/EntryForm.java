package org.myhouse.FuelManager;

import android.content.Intent;
import android.os.Bundle;

import androidx.annotation.NonNull;

import android.view.View;
import android.widget.CalendarView;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

public class EntryForm extends Dashboard
{
     @Override
     protected void onCreate(Bundle savedInstanceState)
     {
          super.onCreate(savedInstanceState);
          setContentView(R.layout.activity_entry_form);

          formattedDate = "";
          milesText = findViewById(R.id.miles);
          gallonsText = findViewById(R.id.gallons);
          costText = findViewById(R.id.cost);
          dateView = findViewById(R.id.date);

          dateView.setOnDateChangeListener(new CalendarView.OnDateChangeListener()
          {
               @Override
               public void onSelectedDayChange(@NonNull CalendarView view, int year, int month, int dayOfMonth)
               {
               String msg = year + "-" + (month + 1) + "-" + dayOfMonth;
               //Toast.makeText(getApplicationContext(),msg,Toast.LENGTH_LONG).show();

               try
               {
                    SimpleDateFormat sdf = new SimpleDateFormat("yyyy-M-dd", Locale.US);
                    Date newdate = sdf.parse(msg);
                    dateView.setDate(newdate.getTime());
               }
               catch (ParseException pe)
               {

               }
               }
          });
          //vehicle = dbase.getVehicleID(prefs.getVehicleID(), prefs.getVehicleLocation());
          updateUI();
     }

     public void saveChanges(View v)
     {
          vehicle = getVehicle();
          if (vehicle.getVehicleId() != -1)
          {
               String msg = "Please fill out: \n";
               boolean ok = true;

               double m = 0.0;
               double g = 0.0;
               double c = 0.0;

               SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd", Locale.US);
               long number = dateView.getDate();
               Date newdate = new Date(number);
               formattedDate = sdf.format(newdate);

               //Toast.makeText(getApplicationContext(), formattedDate, Toast.LENGTH_LONG).show();

               try
               {
                    m = Double.parseDouble(milesText.getText().toString());
               } catch (NumberFormatException nfe)
               {
                    ok = false;
                    msg += "miles\n";
               }
               try
               {
                    g = Double.parseDouble(gallonsText.getText().toString());
               } catch (NumberFormatException nfe)
               {
                    ok = false;
                    msg += "gallons\n";
               }
               try
               {
                    c = Double.parseDouble(costText.getText().toString());
               } catch (NumberFormatException nfe)
               {
                    ok = false;
                    msg += "cost\n";
               }
               if (formattedDate.equals(""))
               {
                    ok = false;
                    msg += "date";
               }

               if (ok)
               {
                    //do the work here
                    Mileage mileageData = new Mileage(m, g, c, formattedDate, vehicle.getVehicleId());
                    long returnval = getDbaseHandle().addData(mileageData, vehicle);
                    if (returnval != -1)
                    {
                         // adding the record worked
                         String t = "miles: " + milesText.getText().toString() + "\n" +
                                 "gallons: " + gallonsText.getText().toString() + "\n" +
                                 "cost: " + costText.getText().toString() + "\n" +
                                 "MPG: " + mileageData.getMpg() + "\n" +
                                 "date: " + formattedDate;
                         Toast.makeText(getApplicationContext(), t, Toast.LENGTH_LONG).show();
                         //clear the fields
                         milesText.setText("");
                         gallonsText.setText("");
                         costText.setText("");
                         milesText.requestFocus();
                    } else
                    {
                         msg = "Adding the record failed!";
                         Toast.makeText(getApplicationContext(), msg, Toast.LENGTH_LONG).show();
                    }
               } else
               {
                    //@here put back later
                    Toast.makeText(getApplicationContext(), msg, Toast.LENGTH_LONG).show();
               }
          }
        else
        {
            Toast.makeText(getApplicationContext(),"select a vehicle first!",Toast.LENGTH_LONG).show();
        }
     }

     private void updateUI()
     {
          vehicle = getVehicle();

          if (vehicle.getVehicleId() == -1)
          {
               Toast.makeText(getApplicationContext(), "select a vehicle first!", Toast.LENGTH_LONG).show();
          }
          else
          {
               TextView t = findViewById(R.id.selectedVehicle);
               t.setText(vehicle.getVehicleDescription());
          }
     }

     @Override
     protected void onActivityResult(int requestCode, int ResultCode, Intent data)
     {
          super.onActivityResult(requestCode, ResultCode, data);
          updateUI();
     }


     private Vehicle vehicle;
     private EditText milesText;
     private EditText gallonsText;
     private EditText costText;
     private CalendarView dateView;
     private String formattedDate;
}