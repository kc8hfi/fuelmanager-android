package org.myhouse.FuelManager;

import android.content.ContentValues;
import android.os.Handler;
import android.os.HandlerThread;
import android.os.Message;
import android.util.Log;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.net.URL;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;

import javax.net.ssl.HttpsURLConnection;


public class RefreshEverythingThread extends HandlerThread
{
     private Handler mWorkerHandler;
     private Handler mResponseHandler;
     private RefreshEverythingThread.Callback mCallback;
     private static final String TAG = RefreshEverythingThread.class.getSimpleName();

     private String hostname;
     private String database;
     private String username;
     private String password;
     private String url;

     private ArrayList<DataObject> everything;

     public RefreshEverythingThread(Handler responseHandler, RefreshEverythingThread.Callback callback)
     {
          super(TAG);
          mResponseHandler = responseHandler;
          mCallback = callback;
          everything = new ArrayList<>();
     }

     public interface Callback
     {
          void refreshEverythingHandler(int responsecode, ArrayList<DataObject> data);
     }


     public void prepareHandler()
     {
          mWorkerHandler = new Handler(getLooper(), new Handler.Callback()
          {
               public boolean handleMessage(Message msg)
               {
                    handleRequest();
                    return true;
               }
          });
     }

     public void queueTask(String host, String db, String u, String p, String ur)
     {
          hostname = host;
          database = db;
          username = u;
          password = p;
          url = ur;
          //Log.d(TAG, "set up all the connection stuff");
          //Log.d(TAG,"url: " + url);
          mWorkerHandler.obtainMessage(1).sendToTarget();
     }


     private void handleRequest()
     {
          DataObject v = getVehicles();
          everything.add(v);
          //getVehicles();

          DataObject m = getMileage();
          everything.add(m);

          final int responseCode = 200;
          mResponseHandler.post(new Runnable()
          {
               @Override
               public void run()
               {
                    mCallback.refreshEverythingHandler(responseCode, everything);
               }
          });
     }



     private DataObject getVehicles()
     {
          String webpage = "https://" + hostname + url;
          DataObject vehicles = new DataObject();
          vehicles.setWhichOne("vehicles");

          //everything.setWebpage(webpage);
          final StringBuilder result = new StringBuilder();
          try
          {
               URL url = new URL(webpage);
               HttpsURLConnection httpclient = (HttpsURLConnection) url.openConnection();
               httpclient.setRequestMethod("POST");


               ContentValues credentials = new ContentValues();
               credentials.put("username", username);
               credentials.put("password", password);
               credentials.put("database", database);
               //set up the request to get the vehicles
               credentials.put("clickme", "vehicles");

               ArrayList<StringBuilder> stuff = new ArrayList<>();
               for(String key:credentials.keySet())
               {
                    StringBuilder t = new StringBuilder();
                    t.append(key);
                    t.append("=");
                    t.append(credentials.get(key));
                    stuff.add(t);
               }

               String sendme = String.join("&",stuff);

               Log.d("refresheverythingthread","send: "+sendme);

               OutputStream os = httpclient.getOutputStream();

               BufferedWriter writer = new BufferedWriter(new OutputStreamWriter(os, StandardCharsets.UTF_8));
               writer.write(sendme);
               writer.flush();
               writer.close();
               os.close();

               final int responseCode = httpclient.getResponseCode();
               if (responseCode == HttpsURLConnection.HTTP_OK)
               {
                    String line;
                    BufferedReader br = new BufferedReader(new InputStreamReader(httpclient.getInputStream()));
                    while ( (line = br.readLine()) != null)
                    {
                         result.append(line);
                    }
               }
          } catch (IOException e)
          {
               e.printStackTrace();
          }
          //everything.setVehicles(result);
          vehicles.setTheData(result);
          return vehicles;
     }
     private DataObject getMileage()
     {
          String webpage = "https://" + hostname + url;
          //everything.setWebpage(webpage);
          DataObject mileage = new DataObject();
          mileage.setWhichOne("fuel_mileage");
          final StringBuilder result = new StringBuilder();
          try
          {
               URL url = new URL(webpage);
               HttpsURLConnection httpclient = (HttpsURLConnection) url.openConnection();
               httpclient.setRequestMethod("POST");

               ContentValues credentials = new ContentValues();
               credentials.put("username", username);
               credentials.put("password", password);
               credentials.put("database", database);
               //set up the request to get the fuel mileage
               credentials.put("clickme", "mileage");

               ArrayList<StringBuilder> stuff = new ArrayList<>();
               for(String key:credentials.keySet())
               {
                    StringBuilder t = new StringBuilder();
                    t.append(key);
                    t.append("=");
                    t.append(credentials.get(key));
                    stuff.add(t);
               }

               String sendme = String.join("&",stuff);

               OutputStream os = httpclient.getOutputStream();
               BufferedWriter writer = new BufferedWriter(new OutputStreamWriter(os, StandardCharsets.UTF_8));
               writer.write(sendme);
               writer.flush();
               writer.close();
               os.close();

               final int responseCode = httpclient.getResponseCode();
               if (responseCode == HttpsURLConnection.HTTP_OK)
               {
                    String line;
                    BufferedReader br = new BufferedReader(new InputStreamReader(httpclient.getInputStream()));
                    while ( (line = br.readLine()) != null)
                    {
                         result.append(line);
                    }
               }
          } catch (IOException e)
          {
               e.printStackTrace();
          }
          //everything.setMileage(result);
          mileage.setTheData(result);
          return mileage;
     }
}