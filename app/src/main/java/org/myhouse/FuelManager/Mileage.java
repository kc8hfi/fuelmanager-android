package org.myhouse.FuelManager;

import android.database.Cursor;
import android.os.Bundle;

import java.text.DecimalFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

class Mileage
{
     public Mileage()
     {
          id = -1;

          miles = 0.0;
          gallons = 0.0;
          cost = 0.0;
          mpg = 0.0;
          date = "";
          vehicle_id = -1;
     }

     public Mileage(double m, double g, double c, String d, int v)
     {
          id = -1;
          miles = m;
          gallons = g;
          cost = c;
          vehicle_id = v;
          String fixed = d;
          try
          {
               SimpleDateFormat df = new SimpleDateFormat("MMMM d, yyyy", Locale.US);
               Date nd = df.parse(d);
               df = new SimpleDateFormat("yyyy-MM-dd", Locale.US);
               if (nd != null)
                    fixed = df.format(nd);
          }
          catch (ParseException pe)
          {
          }

          date = fixed;

          if (m != 0)
          {
               mpg = m / g;
          } else
          {
               mpg = 0.0;
          }
     }

     public Mileage(Cursor c)
     {
          id = c.getInt(0);
          miles = c.getDouble(1);
          gallons = c.getDouble(2);
          cost = c.getDouble(3);
          if (gallons != 0)
          {
               mpg = miles / gallons;
               //String t = String.format(Locale.US,"%.2f",mpg);
               //mpg = Double.parseDouble(t);
          } else
          {
               mpg = 12;
          }
          date = c.getString(4);
          vehicle_id = c.getInt(5);
     }

     public Mileage(Bundle b)
     {
          id = b.getInt("id");
          miles = b.getDouble("miles");
          gallons = b.getDouble("gallons");
          cost = b.getDouble("cost");
          if (gallons != 0)
          {
               //Double t = Double.parseDouble(miles) / Double.parseDouble(gallons);
               //mpg = String.format("%.2f",t);
               mpg = miles / gallons;
          } else
          {
               mpg = 0.0;
          }
          date = b.getString("date");
          vehicle_id = b.getInt("vehicle_id");
     }


     public String toString()
     {
          String s = "";
          s += "id: '" + id + "' ";
          s += "miles: '" + miles + "' ";
          s += "gallons: '" + gallons + "' ";
          s += "cost: '" + cost + "' ";
          s += "mpg: '" + mpg + "' ";
          s += "date: '" + date + "' ";
          s += "vehicle_id: ' " + vehicle_id + "' ";
          return s;
     }

     public Bundle toBundle()
     {
          Bundle b = new Bundle();
          b.putInt("id", id);
          b.putDouble("miles", miles);
          b.putDouble("gallons", gallons);
          b.putDouble("cost", cost);
          b.putString("date", date);
          b.putDouble("mpg", mpg);
          b.putInt("vehicle_id", vehicle_id);
          return b;
     }

     public void setRecordId(int i)
     {
          id = i;
     }

     public void setMiles(Double m)
     {
          miles = m;
     }

     public void setGallons(Double g)
     {
          gallons = g;
     }

     public void setCost(Double c)
     {
          cost = c;
     }

     public void setMpg(Double m)
     {
          mpg = m;
     }

     public void setDate(String d)
     {
          date = d;
     }

     public int getId()
     {
          return id;
     }

     public int getVehicleId()
     {
          return vehicle_id;
     }

     public double getMiles()
     {
          return miles;
     }

     public double getGallons()
     {
          return gallons;
     }

     public double getCost()
     {
          return cost;
     }

     public double getMpg()
     {
          return mpg;
     }

     public String getDate()
     {
          return date;
     }

     public String getMilesFormatted()
     {
          DecimalFormat format = new DecimalFormat("#,###.00");
          //String fixed = format.format(miles);
          //return fixed;
          return format.format(miles);
     }

     public String getGallonsFormatted()
     {
          DecimalFormat gallonsFormat = new DecimalFormat("#,###.00");
          //String f = gallonsFormat.format(gallons);
          return gallonsFormat.format(gallons);
     }

     public String getCostFormatted()
     {
          DecimalFormat costFormat = new DecimalFormat("\u00A4" + "#,###.00");
          String f;
          if (cost != 0.0)
          {
               f = costFormat.format(cost);
          } else
          {
               f = "";
          }
          return f;
     }

     public String getmpgFormat()
     {
          DecimalFormat mpgFormat = new DecimalFormat("0.000");
          //String f = mpgFormat.format(mpg);
          return mpgFormat.format(mpg);
     }

     private int id;
     private double miles;
     private double gallons;
     private double cost;
     private double mpg;
     private String date;
     private final int vehicle_id;

}